import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.junit.Test;

class TestArrayValueProcess {
	
	@Test
	public void loadDataInMatrix_LoadDataInArray_ReturnValueFromArray() throws Exception {
		// Arrange
		ArrayValueProcess arrayvalue = new ArrayValueProcess();
		String input = "1 2 3 4 5 6 7 8 9";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		
		//Act
		System.setIn(in);

		//Assert
		assertEquals(1, arrayvalue.loadDataInMatrix()[0][0]);
		assertEquals(2, arrayvalue.loadDataInMatrix()[0][1]);
		assertEquals(3, arrayvalue.loadDataInMatrix()[0][2]);
		assertEquals(4, arrayvalue.loadDataInMatrix()[1][0]);
		assertEquals(5, arrayvalue.loadDataInMatrix()[1][1]);
		assertEquals(6, arrayvalue.loadDataInMatrix()[1][2]);
		assertEquals(7, arrayvalue.loadDataInMatrix()[2][0]);
		assertEquals(8, arrayvalue.loadDataInMatrix()[2][1]);
		assertEquals(9, arrayvalue.loadDataInMatrix()[2][2]);
	}

	@Test(expected = NumberFormatException.class)
	public void loadDataInMatrix_LoadInvalidDataTypeInArray_ThrowNumberFormatException() throws Exception {
		//Arrange
		ArrayValueProcess arrayvalue = new ArrayValueProcess();
		String input = "RFIdeas 2 3 4 5 6 7 8 9";
		InputStream in = new ByteArrayInputStream(input.getBytes(Charset.defaultCharset()));
		
		//Act
		System.setIn(in);
		
		//Assert
		arrayvalue.loadDataInMatrix();
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void loadDataInMatrix_AccessInvalidIndex_ThrowArrayIndexOutOfBoundException() throws Exception {
		//Arrange
		ArrayValueProcess arrayvalue = new ArrayValueProcess();
		String input = "1 2 3 4 5 6 7 8 9";
		InputStream in = new ByteArrayInputStream(input.getBytes(Charset.defaultCharset()));
		
		//Act
		System.setIn(in);
		
		//Assert
		assertEquals(1, arrayvalue.loadDataInMatrix()[0][4]);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void loadDataInMatrix_InputLessNumberOfValues_ThrowArrayIndexOutOfBoundException() throws Exception {
		//Arrange
		ArrayValueProcess arrayvalue = new ArrayValueProcess();
		String input = "1 2 3 4 5 6 7 8";
		InputStream in = new ByteArrayInputStream(input.getBytes(Charset.defaultCharset()));
		
		//Act
		System.setIn(in);
		
		//Assert
		arrayvalue.loadDataInMatrix();
	}

	@Test
	public void identifyPositionInArray_CalculateSumOfAlternateValue_SumOfAlternateValue() throws Exception {
		//Arrange
		int[][] array = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		
		//Act
		ArrayValueProcess arrayvalue = new ArrayValueProcess();
		
		//Assert
		assertEquals(25, arrayvalue.identifyPositionInArray(array)[0]);
		assertEquals(20, arrayvalue.identifyPositionInArray(array)[1]);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void identifyPositionInArray_InputNumberOfInputInArray_ThrowArrayIndexOutOfBoundException()
			throws Exception {
		//Arrange
		int[][] array = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8 } };
		
		//Act
		ArrayValueProcess arrayvalue = new ArrayValueProcess();
		
		//Assert
		arrayvalue.identifyPositionInArray(array);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void displayResult_InputLessNumberOfValues_ThrowArrayIndexOutOfBoundException() throws Exception {
		//Arrange
		int[] array = { 1 };
		
		//Act
		ArrayValueProcess arrayvalue = new ArrayValueProcess();
		
		//Assert
		arrayvalue.displayResult(array);
	}

}

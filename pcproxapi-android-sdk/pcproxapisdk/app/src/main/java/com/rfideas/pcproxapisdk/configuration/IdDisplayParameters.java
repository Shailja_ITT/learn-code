package com.rfideas.pcproxapisdk.configuration;

public class IdDisplayParameters {
    private  short iFACIDDelim; //If bStripFac & bSndFac & bUseDelFac2Id, this char sent between FAC & ID
    private  short iELDelim;    //If NOT bNoUseELChar, this char sent at end of ID
    private short iDDispLen;    // If bFixLenDsp, ID padded with zeros to this length
    private short iFACDispLen;  // If bFixLenDsp, FAC padded with zeros to this length
    private short iExOutputFormat;

    public short getiFACIDDelim() {
        return iFACIDDelim;
    }

    public void setiFACIDDelim(short iFACIDDelim) {
        this.iFACIDDelim = iFACIDDelim;
    }

    public short getiELDelim() {
        return iELDelim;
    }

    public void setiELDelim(short iELDelim) {
        this.iELDelim = iELDelim;
    }

    public short getiDDispLen() {
        return iDDispLen;
    }

    public void setiDDispLen(short iDDispLen) {
        this.iDDispLen = iDDispLen;
    }

    public short getiFACDispLen() {
        return iFACDispLen;
    }

    public void setiFACDispLen(short iFACDispLen) {
        this.iFACDispLen = iFACDispLen;
    }

    public short getiExOutputFormat() {
        return iExOutputFormat;
    }

    public void setiExOutputFormat(short iExOutputFormat) {
        this.iExOutputFormat = iExOutputFormat;
    }
}

#include "stdafx.h"
#include "CppUnitTest.h"
#include "../BST/BST.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace CppUnit
{

		TEST_CLASS(TestBST)
		{
		public:
			struct node* root = NULL;
			TEST_METHOD(testValueInsertion)
			{
				int value = 2;
				root = NULL;
				MonkWatchingFight tester;
				root = tester.loadValueToBeInserted(2);
				Assert::AreEqual(2, root->data);

			}
			TEST_METHOD(testInsertNewNode)
			{
				MonkWatchingFight tester;
				root = NULL;
				root = tester.insertNewNode(5);
				Assert::AreEqual(5, root->data);
			}

			TEST_METHOD(testHeightofTree)
			{
				MonkWatchingFight tester;
				node *root = NULL;
				root = tester.findLocationToInsertElementIntoTree(root, 6);
				root = tester.findLocationToInsertElementIntoTree(root, 3);
				root = tester.findLocationToInsertElementIntoTree(root, 2);
				root = tester.findLocationToInsertElementIntoTree(root, 7);
				Assert::AreEqual(3, tester.ProcessHeightOfBST(root));
			}
			TEST_METHOD(testFindTreeHeightWhenOnlyRoot)
			{
				MonkWatchingFight tester;
				node *root = NULL;
				root = tester.findLocationToInsertElementIntoTree(root, 6);
				Assert::AreEqual(1, tester.FindHeightOfBST(root));
			}


		};
}
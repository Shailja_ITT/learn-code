#include"stdafx.h"
#include"iostream"
#include"BST.h"

using namespace std;

	struct node * MonkWatchingFight::loadValueToBeInserted(int value)
	{
		root = findLocationToInsertElementIntoTree(root, value);
		return root;
	}

	struct node * MonkWatchingFight::findLocationToInsertElementIntoTree(struct node *root, int value)
	{
		if (root == NULL)
			return (insertNewNode(value));
		else if (value < root->data)
			root->leftChild = findLocationToInsertElementIntoTree(root->leftChild, value);
		else if (value>root->data)
			root->rightChild = findLocationToInsertElementIntoTree(root->rightChild, value);

		return root;
	}

	struct node * MonkWatchingFight::insertNewNode(int value)
	{
		struct node *firstElementIsertionAsRoot = (struct node *)malloc(sizeof(struct node));
		firstElementIsertionAsRoot->data = value;
		firstElementIsertionAsRoot->leftChild = NULL;
		firstElementIsertionAsRoot->rightChild = NULL;
		return firstElementIsertionAsRoot;
	}

	int MonkWatchingFight::FindHeightOfBST(node *root)
	{
		return(ProcessHeightOfBST(root));
	}

	int MonkWatchingFight::ProcessHeightOfBST(struct node *treeNode)
	{
		int l = 0, r = 0;
		if (treeNode == NULL)
			return 0;
		l = ProcessHeightOfBST(treeNode->leftChild);
		r = ProcessHeightOfBST(treeNode->rightChild);
		return max(l, r) + 1;
	}


	int main()
	{
		struct node *root = NULL;
		MonkWatchingFight bst;
		int noOfElements, elementIndex, element, heightOfBST;
		cin >> noOfElements;
		for (elementIndex = 0; elementIndex < noOfElements; elementIndex++)
		{
			cin >> element;
			root = bst.loadValueToBeInserted(element);
		}
		heightOfBST = bst.FindHeightOfBST(root);
		cout << heightOfBST << endl;
		return 0;
	}


package com.rfideas.pcproxapisdk;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;

import java.util.ArrayList;


/**
 * Singleton class which handles Connection of readers.
 */
public class Enumerate{
    private static UsbDevice _usbDevice;
    /**
     * Returns an array with all connected USB readers
     * @param context object of the activity
     * @return an array with handles of all connected usb readers or null if no reader is connected
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);}
     * </pre>
     * @see Enumerate#USBDisconnect()
     */
    public static Reader[] USBConnect(Context context) throws SDKException{
        return getEnumeration(context);
    }

    /**
     * Returns the number of pcProx devices found on this machine from USBConnect()
     * @return Number of devices on bus: 0..127
     */
    public static int GetDevCnt(){
        return getTotalNoOfDevices();
    }

    //TODO: Improve impelementation
    /**
     *Disconnects all the readers
     * @see Enumerate#USBConnect(Context)
     */
    public static void USBDisconnect(){
        disconnect();
    }


    private static Reader[] reader;
    //private static Context _context;
    private static UsbCommunication _usbCommunication;
    private static int totalNoOfDevices;
    private static Enumerate enumerate;

    private Enumerate(){
    }
    static {
        enumerate = new Enumerate();
    }

    private static void init(Context context) throws SDKException {
        _usbCommunication = new UsbCommunication(context);
        ArrayList<UsbDeviceConnection> usbDeviceConnectionList = _usbCommunication.connectUsbDevices();
        totalNoOfDevices = usbDeviceConnectionList.size();

        if (totalNoOfDevices == 0){
            reader = null;
        }
        else {


            reader = new Reader[totalNoOfDevices];
            for (int index = 0; index < totalNoOfDevices; index++) {
                _usbDevice = _usbCommunication.getUsbDeviceList().get(index);
                reader[index] = new Reader(usbDeviceConnectionList.get(index), _usbDevice, _usbCommunication);
            }
        }
    }

    private static Reader[] getEnumeration(Context context) throws SDKException{
        //_context = context;
        enumerate.init(context);
        return reader;
    }
    private static int getTotalNoOfDevices(){
        return totalNoOfDevices;
    }

    private static void disconnect(){
        for (int index = 0; index < getTotalNoOfDevices(); index++)
        {
            UsbInterface usbInterfaces = reader[index].getUsbDevice().getInterface(0);
            UsbDeviceConnection usbDeviceConnection = reader[index].getUsbDeviceConnection();
            usbDeviceConnection.releaseInterface(usbInterfaces);
            usbDeviceConnection.close();
        }
        totalNoOfDevices = 0;
    }
}

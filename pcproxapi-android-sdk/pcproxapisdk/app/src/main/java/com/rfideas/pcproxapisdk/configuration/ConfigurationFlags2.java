package com.rfideas.pcproxapisdk.configuration;

/**
 * Contains methods to alter variables defined under GetFlags2() API
 */
public class ConfigurationFlags2 {

    private boolean bUseLeadChrs;  // Use leading chars in ID KB send
    private boolean bDspHex;       // Display ID as ASCII Hex [not ASCII decimal]
    private boolean bWiegInvData;  // Wiegand data on pins is inverted
    private boolean bUseInvDataF;  // Use the bWiegInvData flag over hardware setting
    private boolean bRevWiegBits;  // Reverse the Wiegand Rx bits
    private boolean bBeepID;       // Beep when ID received
    private boolean bRevBytes;     // Reverse byte order (CSN reader)
    private boolean iPad7;

    public boolean getbUseLeadChrs() {
        return bUseLeadChrs;
    }

    public void setbUseLeadChrs(boolean bUseLeadChrs) {
        this.bUseLeadChrs = bUseLeadChrs;
    }

    public boolean getbDspHex() {
        return bDspHex;
    }

    public void setbDspHex(boolean bDspHex) {
        this.bDspHex = bDspHex;
    }

    public boolean getbWiegInvData() {
        return bWiegInvData;
    }

    public void setbWiegInvData(boolean bWiegInvData) {
        this.bWiegInvData = bWiegInvData;
    }

    public boolean getbUseInvDataF() {
        return bUseInvDataF;
    }

    public void setbUseInvDataF(boolean bUseInvDataF) {
        this.bUseInvDataF = bUseInvDataF;
    }

    public boolean getbRevWiegBits() {
        return bRevWiegBits;
    }

    public void setbRevWiegBits(boolean bRevWiegBits) {
        this.bRevWiegBits = bRevWiegBits;
    }

    public boolean getbBeepID() {
        return bBeepID;
    }

    public void setbBeepID(boolean bBeepID) {
        this.bBeepID = bBeepID;
    }

    public boolean getbRevBytes() {
        return bRevBytes;
    }

    public void setbRevBytes(boolean bRevBytes) {
        this.bRevBytes = bRevBytes;
    }
}

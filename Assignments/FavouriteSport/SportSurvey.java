import java.util.*;
import java.io.*;
 
public class HashEntry {
    static HashMap<String, Integer> sportspopularityhashmap = new HashMap<String, Integer>();
    static CustomisedInput inputstream = new CustomisedInput(System.in);
    static int likesforfootball;
    
    public static void main(String args[] ) throws Exception {
        int numberofenteries = inputstream.readInt();
        
        if(!IsInputValid(numberofenteries)){
               return;
          }
          if(!listPeoplesFavouriteSport(numberofenteries)){
               return;
          }
        
        String mostlikedsport = findMostFavouriteSport(sportspopularityhashmap);
        System.out.println(mostlikedsport);
        System.out.println(likesforfootball);
    }
    
    public static boolean listPeoplesFavouriteSport(int numberOfPeopleConsidered ){
        for(int indexOfPeople = 0 ; indexOfPeople < numberOfPeopleConsidered ; indexOfPeople++ ){
         String nameOfPerson = inputstream.readString();
         String sportLiked = inputstream.readString();
        
         if(!IsNameValid(nameOfPerson,sportLiked))
   {
    return false;
   }
         
         if(sportLiked.equals("football")){
             likesforfootball++;
            }
            addSurveyRecord(sportLiked);
        }
        return true;
    }
    
    public static void addSurveyRecord(String sportsName){
        if(sportspopularityhashmap.containsKey(sportsName)){
            int totalNumberOfPlayers =  sportspopularityhashmap.get(sportsName);
            sportspopularityhashmap.put(sportsName, ++totalNumberOfPlayers);
        }else{
         sportspopularityhashmap.put(sportsName, 1);
        }
    }
    
    public static String findMostFavouriteSport(HashMap<String, Integer> surveyDetails){
        int numberOfPeople = 0;
  String mostLikedSport = null;
        for(Map.Entry<String, Integer> entrysetofsportpopularityhashmap : sportspopularityhashmap.entrySet()){
            if(entrysetofsportpopularityhashmap.getValue() > numberOfPeople){
             mostLikedSport = entrysetofsportpopularityhashmap.getKey();  
             numberOfPeople = entrysetofsportpopularityhashmap.getValue();
            }
        }
        return mostLikedSport;
    }
 
 public static boolean IsInputValid(int numberOfEntries)
 {
  if(numberOfEntries >= 1 && numberOfEntries <= 100000)
   return true;
  else
   return false;
  }
 
 public static boolean IsNameValid(String peopleName, String sportsName)
 {
  if((peopleName.length() >= 1 && peopleName.length() <= 10) && (sportsName.length() >= 1 && sportsName.length() <= 10))
   return true;
  else
   return false;
  }
 
}
 
 
class CustomisedInput {
 
    private InputStream ipStream;
    private byte[] buffer = new byte[1024];
    private int currentchar;
    private int numChars;
    private SpaceCharFilter filter;
 
    public CustomisedInput(InputStream ipStream) {
        this.ipStream = ipStream;
    }
 
    public int read() {
        if (numChars == -1)
            throw new InputMismatchException();
        if (currentchar >= numChars) {
            currentchar = 0;
            try {
                numChars = ipStream.read(buffer);
            } catch (IOException e) {
                throw new InputMismatchException();
            }
            if (numChars <= 0)
                return -1;
        }
        return buffer[currentchar++];
    }
 
 
   public String readString() 
 {
        int character = read();
        while (isSpaceChar(character))
            character = read();
        StringBuilder strngBuilder = new StringBuilder();
        do 
  {
            strngBuilder.appendCodePoint(character);
            character = read();
        } while (!isSpaceChar(character));
        return strngBuilder.toString();
    }
 
 
    public int readInt() {
        int character = read();
        while (isSpaceChar(character))
            character = read();
        int sign = 1;
        if (character == '-') 
  {
            sign = -1;
            character = read();
        }
        int strngBuilder = 0;
        do 
  {
            if (character < '0' || character > '9')
                throw new InputMismatchException();
            strngBuilder *= 10;
            strngBuilder += character - '0';
            character = read();
        } while (!isSpaceChar(character));
        return strngBuilder * sign;
    }
 
 
    public boolean isSpaceChar(int character) {
        if (filter != null)
            return filter.isSpaceChar(character);
        return character == ' ' || character == '\n' || character == '\r' || character == '\t' || character == -1;
    }
 
    public interface SpaceCharFilter {
        boolean isSpaceChar(int ch);
    }
    
}
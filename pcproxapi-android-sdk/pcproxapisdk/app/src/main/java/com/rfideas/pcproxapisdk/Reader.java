package com.rfideas.pcproxapisdk;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;

import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags2;
import com.rfideas.pcproxapisdk.configuration.IdBitCounts;
import com.rfideas.pcproxapisdk.configuration.IdDisplayParameters;
import com.rfideas.pcproxapisdk.configuration.LEDControl;
import com.rfideas.pcproxapisdk.configuration.TimeParameters;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Base class for Readers. Contains all the parameters associated with the reader.
 * An array of Reader is initialized when {@link Enumerate#USBConnect(android.content.Context)} is called.
 * @example
 * <pre>
 *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);}
 * </pre>
 * <p>Call {@link Reader#ReadCfg(int)} to pull configuration of the reader into library memory space.</p>
 */
public class Reader {

    private UsbDevice _usbDevice;
    private UsbDeviceConnection _usbDeviceConnection;
    //private Context _context;
    private UsbCommunication _usbCommunication;
    private LEDControl ledControl;
    private String _partNumber;
    private boolean _isLoon;
    private Configuration[] config;
    private int _maxConfig;
    private short _iCnctDevType;
    private int _luid;
    private int _did;

    protected Reader(UsbDeviceConnection usbDeviceConnection, UsbDevice usbDevice, UsbCommunication usbCommunication) throws SDKException{
        this._usbDevice = usbDevice;
        this._usbDeviceConnection = usbDeviceConnection;
        this._usbCommunication = usbCommunication;
        ledControl = new LEDControl();
        get_maxConfig();
        config = new Configuration[_maxConfig + 1];
        for (int index = 0; index < config.length; index++)
        {
            config[index] = new Configuration();
        }
        getProxPlusCardTypeAndCardPriority();
        GetBeeperVolume(); //To fill _isLoon variable.
        //TODO: Call ReadCfg from Constructor
    }

    protected UsbDevice getUsbDevice() {
        return this._usbDevice;
    }
    protected UsbDeviceConnection getUsbDeviceConnection() {
        return this._usbDeviceConnection;
    }

    //TODO: Make these functions public to expose them in future.
    private int getDid() {
        return _did;
    }

    private void setDid(int did) {
        this._did = did;
    }

    /**
     * Get Logical Unit ID from last {@link Reader#ReadCfg(int)} of device.
     * @return LUID a user defined 16-bit ID (0-65536) to be associated with the
     * current selected device.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      int luid = readers[0].getLuid();
     *     }
     * </pre>
     * @see Reader#setLuid(int)
     */
    public int getLuid() {
        return _luid;
    }

    /**
     * Set Logical unit ID to device.
     * @param luid It is a user defined 16-bit ID (0-65536) to be associated with the current
     * device. When multiple devices are connected they may enumerate in random order and
     * the LUID is the safest way to tell them apart.<p> This does not write the configuration
     * items to the device, just to library memory. Setting the reader to defaults does
     * not erase the LUID.
     * Use {@link Reader#WriteCfg(int)} to write to device memory. Setting the reader to defaults
     * does not erase the LUID.
     * @throws SDKException if there is a failure in USB communication or LUID value is not in the allowed range.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      readers[0].setLuid(65525);
     *      readers[0].WriteCfg(0);}
     * </pre>
     * @see Reader#getLuid()
     */
    public void setLuid(int luid) throws SDKException{
        if (luid >= 0 && luid <= 65535) {
            this._luid = luid;
        }
        else {
            throw new SDKException("LUID value must be between 0 and 65536");
        }

    }

    /**
     * Read the device part number string. This is a 24
     * character string such as "RDR-80581AKU" and null terminated.
     * @return String value of device part number
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      String partNumber = readers[0].getPartNumberString();}
     * </pre>
     */
    public String getPartNumberString() throws SDKException{
        getPartNumber();
        return _partNumber;
    }

    private byte[] sendGetFeatureReport(byte[] outputBuffer) throws SDKException {
        byte[] inputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        if (_usbCommunication.sendFR(outputBuffer, _usbDeviceConnection) < 0
                || _usbCommunication.getFR(inputBuffer, _usbDeviceConnection) < 0) {
            throw new SDKException("Failure in USB Communication with the Reader");
        }
        return inputBuffer;
    }

   /* private static void disconnect(){
        UsbCommunication usbObject = new UsbCommunication(context);
        usbObject.disconnectUSBDevices(this._usbDevice);
    }*/

    /**
     * In Android SDK, we get feature reports as a byte buffer,
     * the values of some fields of reader is out of range of
     * byte, hence, we need to convert obtained byte buffer to
     * int buffer to make the values positive.
     */
    private int[] convertBufferFromByteToInt(byte[] byteBuffer){
        int[] intBuffer = new int[byteBuffer.length];
        for (int index = 0; index < byteBuffer.length; index++){
            intBuffer[index] = byteBuffer[index] & 0x00FF;
        }
        return intBuffer;
    }

    /**
     * Call this function with the number of desired short or long beeps.
     * @param count 1..N beeps, Range is 1..5 for short beeps and 1..2 for long beeps
     * @param longBeep long Beep = true, short beep = false
     * @throws SDKException if count exceeds permissible range
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].BeepNow((byte)5,false);}
     * </pre>
     */
    public void BeepNow(byte count, Boolean longBeep) throws SDKException {
        if (count < 1 || count > 5) {
            throw new SDKException("Invalid count value " + count);
        }
        if (longBeep) {
            if (count > 2){
                throw new SDKException("Count cannot be greater than 2 for long beep");
            }
            count |= 0x80;
        }

        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] inputBuffer;

        outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
        outputBuffer[1] = HexConstants.b_ALL_PROD_SUB_BEEP_NOW;
        outputBuffer[2] = count;

        sendGetFeatureReport(outputBuffer);
    }

    /**
     * Get number of configurations present in the reader. For pcProx Plus dual frequency
     * readers this will be 1 or more and 0 for Legacy non pcProx Plus readers.
     * @return 0..N-1: 0 for non pcProx Plus, 1 or more for pcProx Plus. Here N stands for
     * maximum configuration.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      int maxConfiguration = readers[0].GetMaxConfig();}
     * </pre>
     */
    public int GetMaxConfig() {
        return _maxConfig;
    }


    private void getPartNumber() throws  SDKException{
        char partNumber[] = new char[HexConstants.MAX_PRODUCT_NAME_SIZE];

        byte outputBuffer[] = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte inputBuffer[];

        outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
        outputBuffer[1] = HexConstants.b_ALL_PROD_SUB_GET_PART_NUMBER;

        for (Byte index = 0; index <= HexConstants.MAX_PRODUCT_NAME_SIZE / 8 - 1; index++) {
            outputBuffer[2] = index;
            inputBuffer = sendGetFeatureReport(outputBuffer);

            for (int i = 1; i <= HexConstants.FEATURE_REPORT_BUFFER_SIZE; i++) {
                partNumber[index * HexConstants.FEATURE_REPORT_BUFFER_SIZE + i - 1] = (char) inputBuffer[i - 1];
            }
        }
        int index = 0;
        while(partNumber[index] != '\0'){
            index++;
        }
        partNumber = Arrays.copyOfRange(partNumber,0,index);
        this._partNumber = String.valueOf(partNumber);
    }

    private void get_maxConfig() throws SDKException{
        int numMaxConfig = 0;
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] inputBuffer;

        outputBuffer[0] = HexConstants.b_SETFEATCMD_COMBO;
        outputBuffer[1] = HexConstants.b_SUBCMDREAD_COMBO;

        inputBuffer = sendGetFeatureReport(outputBuffer);

        numMaxConfig = inputBuffer[3] - 1;
        if (numMaxConfig >= 0 && numMaxConfig <= 10) {
            _maxConfig = numMaxConfig;
        } else {
            // bad data from proxPlus
            _maxConfig = 0;
        }
    }

    /**
     * A call to this function pulls the device configuration information into the
     * library memory space to be manipulated by the Get*() and Set*()
     * functions. After altering the data the user must call {@link Reader#WriteCfg(int)} to write
     * the changes back to device so they can take effect.
     * @param configurationIndex 0..N-1, where N stands for maximum configuration.
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);}
     * </pre>
     * @see Reader#WriteCfg(int)
     */
    public void ReadCfg(int configurationIndex) throws SDKException{
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            pcProxSendSelectActiveConfiguration(configurationIndex);
            SendGetProxPlus(configurationIndex);
            readConfigBlock0();
            readConfigBlock1(configurationIndex);
            readConfigBlock2(configurationIndex);
            readConfigBlock3(configurationIndex);
            readConfigBlock4(configurationIndex);
        }
        else {
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    //---------------
    //--- Block 0 ---
    //---------------
    private void readConfigBlock0() throws SDKException{
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        int[] inputBuffer;
        byte[] buffer;

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEFWDATA;

        buffer = sendGetFeatureReport(outputBuffer);
        inputBuffer = convertBufferFromByteToInt(buffer);

        this.setLuid((inputBuffer[0]| (inputBuffer[1] << 8)) & 0x0000FFFF);
        this.setDid((inputBuffer[2] | (inputBuffer[3] << 8)) & 0x0000FFFF);
    }

    //---------------
    //--- Block 1 ---
    //---------------
    private void readConfigBlock1(int configurationIndex) throws SDKException{
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] buffer;
        int[] inputBuffer;

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEIDFRMTPARMS;

        buffer = sendGetFeatureReport(outputBuffer);
        inputBuffer = convertBufferFromByteToInt(buffer);


        this.config[configurationIndex].getIdBitCounts().setiLeadParityBitCnt((short) (inputBuffer[2] & 0x0F));
        this.config[configurationIndex].getIdBitCounts().setiTrailParityBitCnt((short) (inputBuffer[2] >> 4 & 0x0F));
        this.config[configurationIndex].getIdBitCounts().setiIDBitCnt((short) inputBuffer[3]);
        this.config[configurationIndex].getIdBitCounts().setiTotalBitCnt((short) inputBuffer[4]);

        this.config[configurationIndex].getIdDisplayParameters().setiFACDispLen((short) inputBuffer[0]);
        this.config[configurationIndex].getIdDisplayParameters().setiDDispLen((short) (inputBuffer[1]));
        this.config[configurationIndex].getIdDisplayParameters().setiFACIDDelim((short) inputBuffer[5]);
        this.config[configurationIndex].getIdDisplayParameters().setiELDelim((short) inputBuffer[6]);

        this.config[configurationIndex].getConfigurationFlags().setbFixLenDsp((inputBuffer[7] & 0x01) != 0);
        this.config[configurationIndex].getConfigurationFlags().setbFrcBitCntEx((inputBuffer[7] & 0x02) != 0);
        this.config[configurationIndex].getConfigurationFlags().setbStripFac((inputBuffer[7] & 0x04) != 0);
        this.config[configurationIndex].getConfigurationFlags().setbSndFac((inputBuffer[7] & 0x08) != 0);
        this.config[configurationIndex].getConfigurationFlags().setbUseDelFac2Id((inputBuffer[7] & 0x10) != 0);
        this.config[configurationIndex].getConfigurationFlags().setbNoUseELChar((inputBuffer[7] & 0x20) != 0);
        this.config[configurationIndex].getConfigurationFlags().setbSndOnRx((inputBuffer[7] & 0x40) != 0);
        this.config[configurationIndex].getConfigurationFlags().setbHaltKBSnd((inputBuffer[7] & 0x80) != 0);
    }

    //---------------
    //--- Block 2 ---
    //---------------
    private void readConfigBlock2(int configurationIndex) throws SDKException{
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] buffer;
        int[] inputBuffer;

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EETMPARMS;

        buffer = sendGetFeatureReport(outputBuffer);
        inputBuffer = convertBufferFromByteToInt(buffer);

        this.config[configurationIndex].getTimeParameters().setiBitStrmTO((short) (inputBuffer[1] * 4));
        this.config[configurationIndex].getTimeParameters().setiIDHoldTO((short) (inputBuffer[2] * 50));
        this.config[configurationIndex].getTimeParameters().setiIDLockOutTm((short) (inputBuffer[3] * 50));
        this.config[configurationIndex].getTimeParameters().setiUSBKeyPrsTm((short) (inputBuffer[4] * 4));
        this.config[configurationIndex].getTimeParameters().setiUSBKeyRlsTm((short) (inputBuffer[5] * 4));
        this.config[configurationIndex].getTimeParameters().setiTPCfgFlg3((short) (inputBuffer[6]));

        short temp = (short) inputBuffer[6];

        this.config[configurationIndex].getConfigurationFlags3().setbUseNumKP((temp & 0x80) != 0);
        this.config[configurationIndex].getConfigurationFlags3().setbSndSFON((temp & 0x40) != 0);
        this.config[configurationIndex].getConfigurationFlags3().setbSndSFFC((temp & 0x20) != 0);
        this.config[configurationIndex].getConfigurationFlags3().setbSndSFID((temp & 0x10) != 0);
        this.config[configurationIndex].getConfigurationFlags3().setbPrxProEm((temp & 0x08) != 0);
        this.config[configurationIndex].getConfigurationFlags3().setbLowerCaseHex((temp & 0x04) != 0);
        this.config[configurationIndex].getConfigurationFlags3().setbUse64Bit((temp & 0x02) != 0);
        this.config[configurationIndex].getTimeParameters().setExFeatures01((short) (inputBuffer[7]));
    }

    //---------------
    //--- Block 3 ---
    //---------------
    private void readConfigBlock3(int configurationIndex) throws SDKException{
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] buffer;
        int [] inputBuffer;


        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEIDFRMTPARMS2;

        buffer = sendGetFeatureReport(outputBuffer);
        inputBuffer = convertBufferFromByteToInt(buffer);

        this.config[configurationIndex].getConfigurationFlags2().setbUseLeadChrs((inputBuffer[1] & 0x01) != 0);

        GetLEDControl().setbAppCtrlsLED((inputBuffer[1] & 0x02)!= 0);
        GetLEDControl().setiRedLEDState((inputBuffer[0] & 0x01) != 0);
        GetLEDControl().setiGrnLEDState((inputBuffer[0] & 0x02) != 0);

        this.config[configurationIndex].getConfigurationFlags2().setbDspHex((inputBuffer[1] & 0x04) != 0);
        this.config[configurationIndex].getConfigurationFlags2().setbWiegInvData((inputBuffer[1] & 0x08) != 0);
        this.config[configurationIndex].getConfigurationFlags2().setbBeepID((inputBuffer[1] & 0x10) != 0);
        this.config[configurationIndex].getConfigurationFlags2().setbRevWiegBits((inputBuffer[1] & 0x20) != 0);
        this.config[configurationIndex].getConfigurationFlags2().setbRevBytes((inputBuffer[1] & 0x40) != 0);
        this.config[configurationIndex].getConfigurationFlags2().setbUseInvDataF((inputBuffer[1] & 0x80) != 0);

        this.config[configurationIndex].getIdDisplayParameters2().setiCrdGnChr0((short) inputBuffer[2]);
        this.config[configurationIndex].getIdDisplayParameters2().setiCrdGnChr1((short) inputBuffer[3]);


    }

    //---------------
    //--- Block 4 ---
    //---------------
    private void readConfigBlock4(int configurationIndex) throws SDKException{
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] buffer;
        int[] inputBuffer;

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEXPARMS1;
        buffer = sendGetFeatureReport(outputBuffer);
        inputBuffer = convertBufferFromByteToInt(buffer);
        this.config[configurationIndex].getEex1Config().setLeadParityX((short) inputBuffer[0]);
        this.config[configurationIndex].getEex1Config().setTrailParityX((short) inputBuffer[1]);
        //pcprox.cpp line 1192
        this.config[configurationIndex].getIdBitCounts().setiLeadParityBitCnt(
                (short) (this.config[configurationIndex].getIdBitCounts().getiLeadParityBitCnt() + inputBuffer[0]));
        this.config[configurationIndex].getIdBitCounts().setiTrailParityBitCnt(
                (short) (this.config[configurationIndex].getIdBitCounts().getiTrailParityBitCnt() + inputBuffer[1]));
    }

    private void pcProxSendSelectActiveConfiguration(int configurationIndex) throws SDKException, SDKException{
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];

            outputBuffer[0] = HexConstants.b_SETFEATCMD_COMBO;
            outputBuffer[1] = HexConstants.b_SUBCMDWRITE_COMBO;
            outputBuffer[2] = (byte) configurationIndex;

            sendGetFeatureReport(outputBuffer);
        }
        else {
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    private void getProxPlusCardTypeAndCardPriority() throws SDKException{
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] inputBuffer;

        for (int configurationIndex = 0; configurationIndex < config.length; configurationIndex ++) {

            pcProxSendSelectActiveConfiguration(configurationIndex);
            for (int index = 0; index < HexConstants.FEATURE_REPORT_BUFFER_SIZE; index++) {
                outputBuffer[index] = 0;
            }

            outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEXCOMBO;

            inputBuffer = sendGetFeatureReport(outputBuffer);

            int cardType = (inputBuffer[1] | (inputBuffer[2] << 8)) & 0x0000FFFF;
            this.config[configurationIndex].setCardType(cardType);

            int cardPriority = inputBuffer[3] & 1;
            this.config[configurationIndex].setCardPriority(cardPriority);
        }
    }

    /**
     * Get the Card Type 0x0000..0xFFFE for the given configuration.
     * @param configurationIndex 0..N-1, where N stands for maximum configuration.
     * @return 0..0xFFFE for valid card types. -1 for error, disconnected,
     * or non pcProx Plus reader.
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      int cardType = readers[0].GetCardType(0);}
     * </pre>
     * @see Reader#SetCardTypePriority(int, int, int)
     */
    public int GetCardType(int configurationIndex) throws SDKException {
        int cardType = 0;
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            cardType = this.config[configurationIndex].getCardType();
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
        return cardType;
    }

    /**
     * Get the Card Priority for the configuration index. The Priority is 0 or 1, and -1 indicates an error.
     * @param configurationIndex 0..N-1, where N stands for maximum configuration.
     * @return 0 = Low priority, 1 = high priority, -1 error.
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      int cardPriority = GetCardPriority(0);}
     * </pre>
     * @see Reader#SetCardTypePriority(int, int, int)
     */
    public int GetCardPriority(int configurationIndex) throws SDKException {
        int cardPriority = 0;
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            cardPriority = this.config[configurationIndex].getCardPriority();
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
        return cardPriority;
    }
// TODO :add the cardtype txt for reference

    /**
     * Set the Card Type 0x0000..0xFFFF and the card priority for the given configuration index.
     * Card types not understood by the device firmware are ignored and will return as
     * (0x0000) OFF. <p>The priority bit if non-zero will have priority
     * over other configurations that are set to zero. The priority allows dual frequency
     * cards or multiple cards to be read in a predictable manner. <p>Only one configuration
     * should have the priority bit set, otherwise unpredictable results may occur on
     * multiple card reads.<p>It is not applicable for single config readers.</p>
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @param cardType 16 bit card type
     * @param cardPriority 0 for low and 1 for high
     * @throws SDKException if configurationIndex exceeds maximum Config or priority value is other than 0 or 1 or
     * if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      readers[0].SetCardTypePriority(0, 61188, 1);
     *      readers[0].WriteCfg(0);
     *     }
     * </pre>
     *@see Reader#GetCardType(int)
     * @see Reader#GetCardPriority(int)
     */
    public void SetCardTypePriority ( int configurationIndex, int cardType, int cardPriority) throws SDKException {
        //TODO: Handle cardType as enum
        if(configurationIndex <= GetMaxConfig()) {
            if (cardPriority == 0 || cardPriority == 1) {
                this.config[configurationIndex].setCardType(cardType);
                this.config[configurationIndex].setCardPriority(cardPriority);
            }
            else {
                throw new SDKException("card Priority Value must be 0 or 1");
            }
        }
        else
            throw new SDKException("Invalid configuration index" + configurationIndex);
    }

    private boolean SendSetProxPlus(int configurationIndex) throws SDKException, InterruptedException {
        boolean result = true;

        byte outputBuffer[] = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte inputBuffer[];

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEXCOMBO;

        inputBuffer = sendGetFeatureReport(outputBuffer);


        outputBuffer[0] = 0;
        outputBuffer[1] = (byte) ((this.config[configurationIndex].getCardType() & 255));
        outputBuffer[2] = (byte) ((this.config[configurationIndex].getCardType() >> 8) & 255);
        outputBuffer[3] = (byte) (this.config[configurationIndex].getCardPriority() == 0 ? 0 : 1);

        inputBuffer = sendGetFeatureReport(outputBuffer);
        Thread.sleep(10);
        /*for (byte bufferData : inputBuffer) {
            if (bufferData != 0) {
                result = true;
                break;
            }
        }*/



        return result;
    }

    private boolean SendGetProxPlus (int configurationIndex) throws SDKException{
        boolean result = false;
        int cfgIndex = 0;
        byte outputBuffer[] = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte inputBuffer[];

        outputBuffer[0] = HexConstants.b_SETFEATCMD_COMBO;
        outputBuffer[1] = HexConstants.b_SUBCMDREAD_COMBO;

        inputBuffer = sendGetFeatureReport(outputBuffer);

        if (inputBuffer[0] != HexConstants.b_SETFEATCMD_COMBO) {
            return result;
        }
        int N = inputBuffer[3] - 1;
        if(N >= 0 && N <= HexConstants.MAX_PLUS_CONFIG){
            //_maxConfig = N;
            cfgIndex = inputBuffer[2];
            if (cfgIndex > N){
                cfgIndex = N;
                //pcProxSendSelectActiveConfiguration(N);
            }
        }
        else{
            //bad data from proxPlus
            //_maxConfig = 0;
            configurationIndex = 0;
            //pcProxSendSelectActiveConfiguration(0);
            return result;
        }

        //Get more parameters
        // Read pcProxPus parameters in
        for ( int index = 0; index < HexConstants.FEATURE_REPORT_BUFFER_SIZE; index ++){
            outputBuffer[index] = 0;
        }
        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEXCOMBO;
        result = true;
        inputBuffer = sendGetFeatureReport(outputBuffer);
        this.config[configurationIndex].setCardPriority(inputBuffer[3] & 1);
        this.config[configurationIndex].setCardType((inputBuffer[1] | (inputBuffer[2] << 8)) & 0x0000FFFF);
        return result;
    }

//TODO: description
    /**
     * Reads the active data from the card "presently on the reader".
     * This function will return the number of bits read.
     * The buffer passed in arguments is populated with card data.
     * <p>It is recommended you do not call this faster than 250 msec, or about twice the
     * data hold time of the active card. Call sooner than 250 msec will not be sent
     * to the reader but will return cached data.
     * <p>It is valid only for cards less than 64 bits.</p>
     * @param buffer It is an empty int array of desired size. It will be populated with the
     * active data from the card
     * @param bufferMaximumSize It specifies the size of the byte buffer. Maximum value is 8, if the value provided is more
     * than 8, it will automatically reset to 8.
     * @return It returns number of bits read from the reader representing the card ID
     * <p>It does NOT include the parity bits that may have been stripped from the
     * ID through the use of the leading and/or trailing parity bit counts.
     * <p>A return value of zero means there is either no card within RF field or an error was
     * encountered or a wrong card was presented.
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      int buffer[64];
     *      short activeID = readers[0].GetActiveID(buffer, 64);}
     * </pre>
     * @see Reader#GetActiveID32(int[], short)
     */
    public short GetActiveID(int[] buffer, short bufferMaximumSize) throws SDKException{
        short noOfBits = 0;
        short bufferSize = 0;

        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] inputBuffer;

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | 0x0F;

        inputBuffer = sendGetFeatureReport(outputBuffer);

        bufferSize = (bufferMaximumSize < 8) ? bufferMaximumSize : 8;

        for (int index = 0; index < bufferSize; index++){
            buffer[index] = inputBuffer[bufferSize - index - 1] & 0x00FF;
        }

        for (int index = 0; index < HexConstants.FEATURE_REPORT_BUFFER_SIZE; index++){
            outputBuffer[index] = 0;
        }

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | 0x0E;

        inputBuffer = sendGetFeatureReport(outputBuffer);

        noOfBits = inputBuffer[0];

        return noOfBits;
    }

    /**
     * This is the 32 byte version of {@link Reader#GetActiveID(int[], short)} for the readers
     * to get the card ID with up to 255 bits of data. Cards with larger
     * ID's than 64 bits will be truncated with {@link Reader#GetActiveID(int[], short)}, so this function
     * is recommended instead.
     * This function will return the number of bits read.
     * The buffer passed in arguments is populated with card data.
     * <p>It is recommended you do not call this faster than 250 msec, or about twice the
     * data hold time of the active card. Call sooner than 250 msec will not be sent
     * to the reader but will return cached data.
     * @param buffer an empty int array of desired size. It will be populated with the
     * active data from the card
     * @param bufferMaximumSize It specifies the size of the byte buffer. Maximum value is 32, if the value provided is more
     * than 32, it will automatically reset to 32
     * @return Number of bits Read 0..255
     * <p>It does NOT include the parity bits that may have been stripped from the
     * ID through the use of the leading and/or trailing parity bit counts.
     * <p>A return value of zero means there is either no card within RF field or an error was
     * encountered or a wrong card was presented.
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      int buffer[84];
     *      short activeID32Bits = readers[0].GetActiveID32(buffer,84);}
     * </pre>
     * @see Reader#GetActiveID(int[], short)
     */
    public short GetActiveID32(int[] buffer, short bufferMaximumSize) throws SDKException{
        short noOfBits = 0;
        short bufferSize = 0;
        int iFRNum;
        byte ucIDBuf32[] = new byte [64]; // really only need 32 bytes

        bufferSize =  (bufferMaximumSize < 32)? bufferMaximumSize : 32;

        byte outputBuffer[] = new byte [HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte inputBuffer[] ;

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_VRGETID32;

        //--- need to loop 4 times to get entire big ID ---
        for(iFRNum = 0; iFRNum<4; iFRNum++){
            outputBuffer[1] = (byte) iFRNum;
            inputBuffer = sendGetFeatureReport(outputBuffer);
            for(int index = 0; index< HexConstants.FEATURE_REPORT_BUFFER_SIZE; index++ ){
                ucIDBuf32[iFRNum*8+ index] = inputBuffer[index];
            }
        }

        //Now get the detail...
        //Select Block, get from USB...
        for (int index =0; index < HexConstants.FEATURE_REPORT_BUFFER_SIZE; index++){
            outputBuffer[index] = (byte) 0;
        }
        outputBuffer[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_VRGETIDDTL;
        inputBuffer = sendGetFeatureReport(outputBuffer);

        noOfBits = (short) inputBuffer[0];

        for (int index =0; index < bufferSize; index++){
            buffer [index] = ucIDBuf32[bufferSize-index-1] & 0x00FF;
        }

        return noOfBits;
    }


    /**
     * Gets the volume level for the pcProx Plus(version 2).
     * @return Volume level 0..3 for the pcProx Plus(version 2) Reader.
     * <ul><li>0- off</li>
     * <li>1- low</li>
     * <li>2- med</li>
     * <li>3- High</li>
     * <li>-1- For non pcProx Plus(version 2)</li></ul>
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      short volumeLevel = readers[0].GetBeeperVolume();}
     * </pre>
     * @see Reader#SetBeeperVolume(byte)
     */
    public short GetBeeperVolume() throws SDKException{
        short volumeLevel = -1;
        _isLoon = false;

        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] inputBuffer;

        outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
        outputBuffer[1] = HexConstants.b_ALL_PROD_SUB_GET_BEEPER_VOLUME;

        inputBuffer = sendGetFeatureReport(outputBuffer);
        //For a non-Loon reader response would be zero but if a loon reader the response would be non-zero.
        for (byte bufferData : inputBuffer ){
            if(bufferData != 0){
                _isLoon = true;
                break;
            }
        }
        if(_isLoon){
            volumeLevel = inputBuffer[2];
        }
        return volumeLevel;
    }

    /**
     * Sets the volume level for the pcProx Plus (version 2)(loon) reader.
     * @param volumeLevel volumeLevel in range 0..3,
     * <ul><li>0-off</li>
     * <li>1-low</li>
     * <li>2-med</li>
     * <li>3-High</li></ul>
     * @return true if successful, false otherwise
     * @throws SDKException if Volume level is less than zero or greater than 3
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      readers[0].SetBeeperVolume((byte)2);
     *      readers[0].WriteCfg(0);}
     * </pre>
     * @see Reader#GetBeeperVolume()
     */
    public boolean SetBeeperVolume (byte volumeLevel) throws SDKException{
        if(isLoon()) {
            boolean brc = false;
            if (volumeLevel < 0 || volumeLevel > 3) {
                throw new SDKException("Invalid Volume Level " + volumeLevel);
            }
            byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
            byte[] inputBuffer;

            outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
            outputBuffer[1] = HexConstants.b_ALL_PROD_SUB_SET_BEEPER_VOLUME;
            outputBuffer[2] = volumeLevel;

            inputBuffer = sendGetFeatureReport(outputBuffer);

            //For a non-Loon reader response would be zero but if a loon reader the response would be non-zero.
            for (byte bufferData : inputBuffer) {
                if (bufferData != 0) {
                    brc = true;
                    break;
                }
            }
            return brc;
        }
        throw new SDKException("API is available only for loon readers");
    }


    /**
     * Checks whether a reader is loon or not.
     * @return true for loon readers, false otherwise
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      boolean _isDeviceLoon = readers[0].isLoon();}
     * </pre>
     */
    public boolean isLoon(){
        return _isLoon;
    }

    /**
     * Reads the device firmware filename.
     * <p>e.g. <b>"LNC160700UBX700"</b>. Works only for loon readers.
     * @return String value of Firmware Filename on success
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      String filename = readers[0].GetFWFilename();}
     * </pre>
     */
    public String GetFWFilename() throws SDKException {
        if (isLoon()) {
            String filename;
            char[] filenameBuffer = new char[HexConstants.MAXIMUM_FWFILENAME_LENGTH];
            byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
            byte[] inputBuffer;

            outputBuffer[0] = HexConstants.b_ALL_PROD_USB; //0x8c;
            outputBuffer[1] = HexConstants.b_ALL_PROD_SUB_GETSTRING;
            outputBuffer[2] = 0x07;

            //Number of iteration = 7, as mentioned in the document.
            for (byte stringChunk = 0; stringChunk <= 7; stringChunk++) {
                outputBuffer[3] = stringChunk;

                inputBuffer = sendGetFeatureReport(outputBuffer);

                for (int index = 0; index < HexConstants.FEATURE_REPORT_BUFFER_SIZE; index++) {
                    filenameBuffer[stringChunk * 8 + index] = (char) inputBuffer[index];
                }
            }
            int index = filenameBuffer.length - 1;
            while (filenameBuffer[index] == '\0') {
                index--;
            }
            filenameBuffer = Arrays.copyOfRange(filenameBuffer, 0, index + 1);
            filename = String.valueOf(filenameBuffer);
            return filename;
        }
        throw new SDKException("API only works for loon readers");
    }

    /**
     * Reads the device's full Firmware version including the String version number.
     * <p>e.g. <b>"16.07.0-7715"</b>. Works only for loon readers.
     * @return Returns the Firmware version with the string version number for loon readers having version
     * number 16.7 or greater.
     * In case of failure it returns Null.
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      String fullFWVersion = readers[0].GetFullFWVersion();}
     * </pre>
     */
    public String GetFullFWVersion () throws SDKException {
        if (isLoon()) {
            String fullFWVersion;
            char[] fwVersionBuffer = new char[HexConstants.MAXIMUM_FWNAME_LENGTH];
            byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
            byte[] inputBuffer;

            outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
            outputBuffer[1] = HexConstants.b_ALL_PROD_SUB_GETSTRING;
            outputBuffer[2] = 0x04; //Version String

            //Total number of iterations = 3 as full FWVersion will not take more than 24 bytes of data.
            for (byte stringChunk = 0; stringChunk < 3; stringChunk++) {
                outputBuffer[3] = stringChunk;

                inputBuffer = sendGetFeatureReport(outputBuffer);

                for (int index = 0; index < HexConstants.FEATURE_REPORT_BUFFER_SIZE; index++)
                    fwVersionBuffer[stringChunk * 8 + index] = (char) inputBuffer[index];
            }
            int index = fwVersionBuffer.length - 1;
            while (fwVersionBuffer[index] == '\0') {
                index--;
            }
            fwVersionBuffer = Arrays.copyOfRange(fwVersionBuffer, 0, index + 1);
            fullFWVersion = String.valueOf(fwVersionBuffer);
            return fullFWVersion;
        }
        throw new SDKException("API works only for Loon Readers with firmware version 16.7 or higher");
    }

    /**Returns {@link ConfigurationFlags} object for the given configuration index. {@link ConfigurationFlags} contains:
     * <ul><li>boolean bFixLenDsp:<p>It will return whether the send fixed length FAC/ID is set or not</p></li>
     * <li>boolean bFrcBitCntEx:  <p>It will return whether the read bit count to be exact to
     * be valid is set or not</p></li>
     * <li>boolean bStripFac:  <p>It returns whether the stripping of the FAC from the
     * ID (not discarded) is set or not</p></li>
     * <li>boolean bSndFac:  <p>It returns whether the send FAC (if
     * stripped from data) is set or not</p></li>
     * <li>boolean bUseDelFac2Id:  <p>It gives the delimiter placed between FAC and ID on send</p></li>
     * <li>boolean bNoUseELChar:  <p>It gives the termination keystroke set i.e. the last
     * character after card ID (default to ENTER)</p></li>
     * <li>boolean bSndOnRx:  <p>It sends valid ID as soon as it is received
     * (iIDLockOutTm timer not used)</p></li>
     * <li>boolean bHaltKBSnd:  <p>When set true,it dies not send keys to USB i.e.
     * itdisables key strokes
     * </li></ul>
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @return {@link ConfigurationFlags} object
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      ConfigurationFlags configurationFlags = readers[0].GetFlags(0);
     *      boolean isFixLengthDisplay = configurationFlags.getbFixLenDsp();}
     * </pre>
     * @see Reader#SetFlags(ConfigurationFlags, int)
     */
    public ConfigurationFlags GetFlags(int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            return this.config[configurationIndex].getConfigurationFlags();
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Sets {@link ConfigurationFlags} object for the given configuration index. {@link ConfigurationFlags} contains:
     * <ul><li>boolean bFixLenDsp:<p>If set to true it will set the FAC digits and the ID digits to
     * the number specified by the user </p></li>
     * <li>boolean bFrcBitCntEx:  <p>It forces the read bit count to be exact to
     * be valid</p></li>
     * <li>boolean bStripFac:  <p>It sets the stripping of the FAC from the
     * ID (not discarded)</p></li>
     * <li>boolean bSndFac:  <p>When true, it allows to Send the FAC (if
     * stripped from data)</p></li>
     * <li>boolean bUseDelFac2Id:  <p>It puts a delimiter between FAC and ID on send</p></li>
     * <li>boolean bNoUseELChar:  <p>It sets the termination keystroke i.e. the last
     * character after card ID (default to ENTER)</p></li>
     * <li>boolean bSndOnRx:  <p>It sends valid ID as soon as it is received
     * (iIDLockOutTm timer not used)</p></li>
     * <li>boolean bHaltKBSnd:  <p>When set true,it dies not send keys to USB i.e.
     * itdisables key strokes
     * </li></ul>
     * @param configurationFlags ConfigurationFlags object
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @throws SDKException if configurationIndex exceeds maximum Config or
     * if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      ConfigurationFlags configurationFlags = readers[0].GetFlags(0);
     *      configurationFlags.bHaltKBSnd(false);
     *      readers[0].SetFlags(configurationFlags,0);
     *      readers[0].WriteCfg(0);
     *      }
     * </pre>
     * @see Reader#GetFlags(int)
     */
    public void SetFlags(ConfigurationFlags configurationFlags, int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            this.config[configurationIndex].setConfigurationFlags(configurationFlags);
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Sets {@link ConfigurationFlags2} object for the given configuration index. {@link ConfigurationFlags2} contains:
     * <ul><li>boolean bUseLeadChrs: <p>It is set to Use leading chars in ID KB send</p></li>
     * <li>boolean bDspHex: <p>It is set to display ID as ASCII Hex [not ASCII
     * decimal]l</p></li>
     * <li>boolean bWiegInvData: <p>It is set if wiegand data on pins is inverted</p></li>
     * <li>boolean bUseInvDataF: <p>It is set to use the bWiegInvData flag over
     * hardware setting</p></li>
     * <li>boolean bRevWiegBits: <p>It is set to reverse the Wiegand read bits</p></li>
     * <li>boolean bBeepID: <p>It is set to let the device beep when ID is received</p></li>
     * <li>boolean bRevBytes: <p>It is set to reverse the byte order (CSN reader)</p>
     * </li></ul>
     * @param configurationFlags2 ConfigurationFlags2 object
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      ConfigurationFlags2 configurationFlags2 = readers[0].GetFlags2(0);
     *      configurationFlags2.bRevWiegBits(true);
     *      readers[0].SetFlags2(configurationFlags2,0);
     *      readers[0].WriteCfg(0);
     *      }
     * </pre>
     * @see Reader#GetFlags2(int)
     */
    public void SetFlags2(ConfigurationFlags2 configurationFlags2, int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            this.config[configurationIndex].setConfigurationFlags2(configurationFlags2);
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Sets {@link IdBitCounts} object for the given configuration index. {@link IdBitCounts} contains:
     * <ul><li>short iLeadParityBitCnt: <p>It strips the most significant bits from the output of
     * the card, as per the value passed in arguments. The value should be in range 0..15 for single config readers,
     * 0..142 otherwise</p></li>
     * <li>short iTrailParityBitCnt: <p> It strips the least significant bits from the output of
     * the card, as per the value passed in arguments. The value should be in range 0..15 for single config readers,
     * 0..142 otherwise</p></li>
     * <li>short iIDBitCnt:  <p>It sets the id field bit count as per the argument passed with it. The value
     * should be in range 1..255</p></li>
     * <li>short iTotalBitCnt:  <p>It sets the value in argument to the total bits that can only be read by the card.
     * The value should be in range 26..255</p></li></ul>
     * @param idBitCounts IdBitCounts object
     * @param configurationIndex 0..N-1, N stands for maximum configuration
     * @throws SDKException if configurationIndex exceeds maximum Config or
     * input values exceeds the allowed range or  if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      IdBitCounts idBitCnts = readers[0].GetIDBitCnts(0);
     *      idBitCnts.setiTotalBitCnt((short) 0);
     *      readers[0].SetIDBitCnts(idBitCnts,0);
     *      readers[0].WriteCfg(0);
     *      }
     * </pre>
     * @see Reader#GetIDBitCnts(int)
     */
    public void SetIDBitCnts(IdBitCounts idBitCounts, int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            if((this.config[configurationIndex].getTimeParameters().getExFeatures01() & 0x0080) != 0) {
                if(idBitCounts.getiLeadParityBitCnt() > 142 || idBitCounts.getiLeadParityBitCnt() < 0) {
                    throw new SDKException("LP must be betweeen 0 and 142");
                }
                if(idBitCounts.getiTrailParityBitCnt() > 142 || idBitCounts.getiTrailParityBitCnt() < 0) {
                    throw new SDKException("TP must be betweeen 0 and 142");
                }
            }
            else
            {
                if(idBitCounts.getiLeadParityBitCnt() > 15 || idBitCounts.getiLeadParityBitCnt() < 0) {
                    throw new SDKException("LP must be betweeen 0 and 15");
                }
                if(idBitCounts.getiTrailParityBitCnt() > 15 || idBitCounts.getiTrailParityBitCnt() < 0) {
                    throw new SDKException("TP must be betweeen 0 and 15");
                }
            }
            this.config[configurationIndex].setIdBitCounts(idBitCounts);
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Sets {@link TimeParameters} for the selected Configuration Index. {@link TimeParameters} contains:
     * <ul><li>short iBitStrmTO: <p> Wiegand read T/O after this msec time (4ms)</p></li>
     * <li>short iIDHoldTO:  <p>Card ID valid for this msec time (48ms)</p></li>
     * <li>short iIDLockOutTm:  <p>Squelch repetitive reader reports (usually > 1000) in msec (48msec gran.)</p></li>
     * <li>short iUSBKeyPrsTm:  <p>Sets USB inter-key 'Press' time in 4ms units</p></li>
     * <li>short iUSBKeyRlsTm:  <p>Sets USB inter-key 'Release' time in 4ms units</p></li>
     * <li>short ExFeatures01:  <p>Extended Features (big parity Azery ext precision)</p></li>
     * @param timeParameters Object
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @throws SDKException if configurationIndex exceeds maximum Config or
     * if input values are out of allowed range or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      TimeParameters timeParameters = readers[0].GetTimeParms(0);
     *      timeParameters.setiIDHoldTO((short) 900);
     *      readers[0].SetTimeParms(timeParameters, 0);
     *      readers[0].WriteCfg(0);
     *      }
     * </pre>
     * @see Reader#GetTimeParms(int)
     */
    public void SetTimeParms(TimeParameters timeParameters, int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            this.config[configurationIndex].setTimeParameters(timeParameters);
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Returns {@link ConfigurationFlags2} object for the given configuration index. {@link ConfigurationFlags2} contains:
     * <ul><li>boolean bUseLeadChrs: <p>It returns whether the Use leading chars in ID KB send is set or not</p></li>
     * <li>boolean bDspHex: <p>It returns whether display ID as ASCII Hex [not ASCII
     * decimal] is set or not</p></li>
     * <li>boolean bWiegInvData: <p>It returns whether the wiegand data on pins is inverted or not</p></li>
     * <li>boolean bUseInvDataF: <p>It returns whether the bWiegInvData flag over
     * hardware setting is set or not</p></li>
     * <li>boolean bRevWiegBits: <p>It returns whether the reverse the Wiegand read bits is set or not</p></li>
     * <li>boolean bBeepID: <p>It returns whether the beep when ID is received is set or not</p></li>
     * <li>boolean bRevBytes: <p>It returns whether the reverse the byte order (CSN reader) is set or not</p>
     * </li></ul>
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @return {@link ConfigurationFlags2} object
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      ConfigurationFlags2 configurationFlags2 = readers[0].GetFlags2(0);
     *      boolean isDspHex = configurationFlags2.getbDspHex();
     *     }
     * </pre>
     * @see Reader#SetFlags2(ConfigurationFlags2, int)
     */
    public ConfigurationFlags2 GetFlags2(int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            return this.config[configurationIndex].getConfigurationFlags2();
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Returns {@link IdBitCounts} object for the given configuration index. {@link IdBitCounts} contains:
     * <ul><li>short iLeadParityBitCnt:<p>It gives the number of most significant bits stripped.
     * The number should be in range 0..15 for single config readers, 0..142 otherwise</p></li>
     * <li>short iTrailParityBitCnt:<p>It gives the number of least significant bits stripped.
     * The number should be in range 0..15 for single config readers, 0..142 otherwise</p></li>
     * <li>short iIDBitCnt:<p>It gives the value of id bit field count which should be in range 0..255</p></li>
     * <li>short iTotalBitCnt:<p>It gives the value of the total bit counts that can be read by the device.
     * The value should be in range 26..255</p></li></ul>
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @return {@link IdBitCounts} object
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      IdBitCounts idBitCnts = readers[0].GetIDBitCnts(0);
     *      short lp = idBitCnts.getiLeadParityBitCnt();}
     * </pre>
     * @see Reader#SetIDBitCnts(IdBitCounts, int)
     */
    public IdBitCounts GetIDBitCnts(int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            return this.config[configurationIndex].getIdBitCounts();
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Returns {@link TimeParameters} object for the given Configuration index. {@link TimeParameters} contains:
     * <ul><li>short iBitStrmTO: <p>Wiegand read T/O after this msec time (4ms).
     * The range should be 0..1020</p></li>
     * <li>short iIDHoldTO: <p>Card ID valid for this msec time (48ms).
     * The range should be 0..12750</p></li>
     * <li>short iIDLockOutTm: <p>Squelch repetitive reader reports (usually > 1000)
     * in msec (48msec gran.). The range should be 0..12750</p></li>
     * <li>short iUSBKeyPrsTm: <p>It sets USB inter-key 'Press' time in 4ms units.
     * The range should be 0..1020 </p></li>
     * <li>short iUSBKeyRlsTm: <p>It sets USB inter-key 'Release' time in 4ms units. The range should be 0..1020</p></li>
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @return {@link TimeParameters} object
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      TimeParameters timeParameters = readers[0].GetTimeParms(0);
     *      short holdTime = timeParameters.getiIDHoldTO();}
     * </pre>
     * @see Reader#SetTimeParms(TimeParameters, int)
     */
    public TimeParameters GetTimeParms(int configurationIndex) throws SDKException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            return this.config[configurationIndex].getTimeParameters();
        }
        else{
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    /**
     * Get {@link LEDControl} information color red, green, amber, auto or off. {@link LEDControl} Contains:
     * <ul><li>boolean bAppCtrlsLED:<p>It gives true if the control is set to auto mode</p>
     * <li>boolean iRedLEDState:<p>It gives true if the LED state is set to Red color</p></li>
     * <li>boolean iGrnLEDState:<p>It gives true if the LED state is set to Green color</p> </li>
     * <li><p>Color of LED is AMBER when both iGRnLEDState and iRedLEDState are true and bAppCtrlsLED is false</p></li>
     * <li><p></p>LED is turned OFF when bAppCtrlsLED, iGRnLEDState and iRedLEDState are false</p></li></ul>
     * @return {@link LEDControl} Object
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      LEDControl ledControl = readers[0].GetLEDControl();
     *      boolean redLEDState = ledControl.getiRedLEDState();
     *      }
     * </pre>
     * @see Reader#SetLEDControl(LEDControl)
     */
    public LEDControl GetLEDControl () throws SDKException{
        return ledControl;
    }

    /**
     * Sets {@link LEDControl} information color red, green, amber, auto or off for the given configuration.
     * LEDControl Contains:
     * <ul><li>boolean bAppCtrlsLED:<p>It sets the control to auto mode</p>
     * <li>boolean iRedLEDState:<p>It sets the LED state to Red color</p></li>
     * <li>boolean iGrnLEDState:<p>It sets the LED state to Green color</p> </li>
     * <li><p>Setting both iGRnLEDState and iRedLEDState to true and bAppCtrlsLED to false changes
     * the LED to AMBER</p></li>
     * <li><p>Setting both iGRnLEDState and iRedLEDState to false will turn the LED OFF</p></li></ul>
     * @param ledControl LEDControl object
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      LEDControl ledControl = readers[0].GetLEDControl();
     *      ledControl.bAppCtrlsLED(true);
     *      ledControl.iGrnLEDState(true);
     *      readers[0].SetLEDControl(ledControl);
     *      readers[0].WriteCfg(0);}
     * </pre>
     * @see Reader#GetLEDControl()
     */
    public void SetLEDControl (LEDControl ledControl) throws SDKException {
        this.ledControl = ledControl;
    }

    /**
     * This API is used to get the device name.
     * <p>e.g. <b></b>"/dev/bus/usb/001/002"</b>
     * @return Device Name String
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      String deviceName = readers[0].GetDevName();}
     * </pre>
     */
    public String GetDevName(){
        return _usbDevice.getDeviceName();
    }

    /**
     * A call to this function writes all configuration information in the library memory
     * space to the device for non-volatile storage. Any changed parameters take effect
     * immediately after {@link Reader#WriteCfg(int)}.
     * <p>The actual write internally within the device is not
     * done until all critical pending actions are complete. This may take up to two
     * seconds, typically 1200 msec to complete.
     * <p>pcProx Plus(version 2) readers takes only 20ms to complete the write.
     * @param configurationIndex 0..N-1, where N stands for maximum configuration
     * @throws SDKException if configurationIndex exceeds maximum Config or if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      readers[0].WriteCfg(0);}
     * </pre>
     * @see Reader#ReadCfg(int)
     */
    //TODO: Add flags for optimisation
    public void WriteCfg(int configurationIndex) throws SDKException, InterruptedException {
        if (configurationIndex >= 0 && configurationIndex <= GetMaxConfig()) {
            pcProxSendSelectActiveConfiguration(configurationIndex);
            SendSetProxPlus(configurationIndex);
            writeConfigBlock0();
            writeConfigBlock1(configurationIndex);
            writeConfigBlock2(configurationIndex);
            writeConfigBlock3(configurationIndex);
            writeConfigBlock4(configurationIndex);
            SendSetProxPlus(configurationIndex);
            writeToEE();
            //Sleep for time to commit
            if (isLoon()) {
                Thread.sleep(HexConstants.TIMETOCOMMIT_FOR_LOON);
            }
            else {
                Thread.sleep(HexConstants.TIMETOCOMMIT);
            }
            //TODO Check line 832 of pcprox.cpp
        } else {
            throw new SDKException("Invalid Configuration Index " + configurationIndex);
        }
    }

    //---------------
    //--- Block 0 ---
    //---------------

    //This method writes the values of LUID and DID
    private void writeConfigBlock0() throws SDKException {
        byte[] outputBufferCommand = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] outputBufferData = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        //DIRTY luid
        int luid = getLuid();
        int did = getDid();
        if (did < 0x0300) {//version dependent behaviour //DISABLE_lEGACY_iDID
            outputBufferData[0] = (byte) (luid & 0x007f);//make sure we don't have the cmd bit set
            outputBufferData[1] = (byte) ((luid >> 8) & 0x00ff);
        } else {
            outputBufferData[1] = (byte) (luid & 0x00ff);
            outputBufferData[2] = (byte) ((luid >> 8) & 0x00ff);
        }
        outputBufferCommand[0] = (byte) HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEFWDATA;

        sendGetFeatureReport(outputBufferCommand);
        sendGetFeatureReport(outputBufferData);
        //DIRTY Luid
    }

    //---------------
    //--- Block 1 ---
    //---------------
    //Writes values of IDBitCounts, ConfigurationFlags, IdDisplayParameters
    private void writeConfigBlock1(int configurationIndex) throws SDKException {
        //DIRTY IDFRmtParamsDirty
        byte[] outputBufferCommand = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] outputBufferData = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];

        IdBitCounts idBitCounts = GetIDBitCnts(configurationIndex);

        short lp = idBitCounts.getiLeadParityBitCnt();
        short tp = idBitCounts.getiTrailParityBitCnt();
        short temp1 = 0, temp2 = 0;
        if (getDid() >= 600 || true) {
            if(lp > 15)
            {
                outputBufferData[2] = 15;
                temp1 = (short)(lp - 15);
            }
            else
            {
                outputBufferData[2] = (byte)lp;
            }
            if(tp > 15)
            {
                outputBufferData[2] |= 0xF0;

                temp2 = (short)(tp - 15);
            }
            else
            {
                outputBufferData[2] |= (byte)((tp << 4) & 0xF0);//this is eex1config
            }
            this.config[configurationIndex].getEex1Config().setLeadParityX(temp1);
            this.config[configurationIndex].getEex1Config().setTrailParityX(temp2);
        }
        else {
            outputBufferData[2] = (byte)(lp & 0x0F);
            outputBufferData[2] |= ((tp << 4) & 0xF0);
        }

        outputBufferData[3] = (byte)(idBitCounts.getiIDBitCnt() & 0x007F);
        outputBufferData[4] = (byte)(idBitCounts.getiTotalBitCnt() & 0x00FF);

        IdDisplayParameters idDisplayParameters = config[configurationIndex].getIdDisplayParameters();

        outputBufferData[0] = (byte)(idDisplayParameters.getiFACDispLen() & 0x007F);
        outputBufferData[1] = (byte)(idDisplayParameters.getiDDispLen() & 0x007F);
        outputBufferData[5] = (byte)(idDisplayParameters.getiFACIDDelim() & 0x00FF);
        outputBufferData[6] = (byte)(idDisplayParameters.getiELDelim() & 0x00FF);


        ConfigurationFlags configurationFlags = GetFlags(configurationIndex);

        short flag = 0;

        flag |= configurationFlags.getbFixLenDsp() ? 0x01: 0;
        flag |= configurationFlags.getbFrcBitCntEx() ? 0x02: 0;
        flag |= configurationFlags.getbStripFac() ? 0x04: 0;
        flag |= configurationFlags.getbSndFac() ? 0x08: 0;
        flag |= configurationFlags.getbUseDelFac2Id() ? 0x10: 0;
        flag |= configurationFlags.getbNoUseELChar() ? 0x20: 0;
        flag |= configurationFlags.getbSndOnRx() ? 0x40: 0;
        flag |= configurationFlags.getbHaltKBSnd() ? 0x80: 0;

        outputBufferData[7] = (byte)flag;

        outputBufferCommand[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEIDFRMTPARMS;

        sendGetFeatureReport(outputBufferCommand);
        sendGetFeatureReport(outputBufferData);
        //DIRTY IDFRmtParamsDirty
    }

    //---------------
    //--- Block 2 ---
    //---------------
    //writes value of TimeParams,
    private void writeConfigBlock2(int configurationIndex) throws SDKException {
        byte[] outputBufferCommand = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] outputBufferData = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
//DIRTY bTimeParamsDirty
        outputBufferData[1] = (byte) ((config[configurationIndex].getTimeParameters().getiBitStrmTO()) / 4);
        outputBufferData[2] = (byte) ((config[configurationIndex].getTimeParameters().getiIDHoldTO()) / 50);
        outputBufferData[3] = (byte) ((config[configurationIndex].getTimeParameters().getiIDLockOutTm()) / 50);
        outputBufferData[4] = (byte) ((config[configurationIndex].getTimeParameters().getiUSBKeyPrsTm()) / 4);
        outputBufferData[5] = (byte) ((config[configurationIndex].getTimeParameters().getiUSBKeyRlsTm()) / 4);
        outputBufferData[6] = (byte) config[configurationIndex].getTimeParameters().getiTPCfgFlg3();
        outputBufferData[7] = (byte) config[configurationIndex].getTimeParameters().getExFeatures01();
        // CfgFlags3 mapped to Time structure


        outputBufferCommand[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EETMPARMS;
        sendGetFeatureReport(outputBufferCommand);
        sendGetFeatureReport(outputBufferData);
        //DIRTY bTimeParamsDirty end
    }

    //---------------
    //--- Block 3 ---
    //---------------
    //writes value of ConfigurationFlags2, LecControl, IdDisplayParameters2, IdDisplayParameters3
    private void writeConfigBlock3(int configurationIndex) throws SDKException {
        //bIDFrmtParms2Dirty start


        byte[] outputBufferCommand = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] outputBufferData = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];


        byte flag = 0;

        flag |= GetLEDControl().getiRedLEDState() ? 0x01 : 0;
        flag |= GetLEDControl().getiGrnLEDState() ? 0x02 : 0;
        //TODO: see if these two lines are needed
        //flag |= pProxCfg->BprRlyCtrl.iRelayState? 0x04: 0;
        //flag |= pProxCfg->BprRlyCtrl.iBeeperState? 0x08: 0;
        flag |= config[configurationIndex].getConfigurationFlags3().getbNotBootDev() ? 0x10 : 0;

        outputBufferData[0] = flag;
        outputBufferData[2] = (byte) (config[configurationIndex].getIdDisplayParameters2().getiCrdGnChr0() & 0x00ff);
        outputBufferData[3] = (byte) (config[configurationIndex].getIdDisplayParameters2().getiCrdGnChr1() & 0x00ff);

        byte leadCharacterCount = (byte) config[configurationIndex].getIdDisplayParameters2().getiLeadChrCnt();
        byte trailCharacterCount = (byte) config[configurationIndex].getIdDisplayParameters3().getiTrailChrCnt();
        if ((leadCharacterCount + trailCharacterCount) > 3) {
            if (leadCharacterCount > 3) {
                leadCharacterCount = 3;
            }
            trailCharacterCount = (byte) (3 - leadCharacterCount);
        }
        outputBufferData[4] = (byte) (leadCharacterCount | (trailCharacterCount << 4));

        flag = 0;
        flag |= config[configurationIndex].getConfigurationFlags2().getbUseLeadChrs() ? 0x01 :0;
        flag |= GetLEDControl().getbAppCtrlsLED() ? 0x02 : 0;
        flag |= config[configurationIndex].getConfigurationFlags2().getbDspHex() ? 0x04 : 0;
        flag |= config[configurationIndex].getConfigurationFlags2().getbWiegInvData() ? 0x08 : 0;
        flag |= config[configurationIndex].getConfigurationFlags2().getbBeepID() ? 0x10 : 0;
        flag |= config[configurationIndex].getConfigurationFlags2().getbRevWiegBits() ? 0x20 : 0;
        flag |= config[configurationIndex].getConfigurationFlags2().getbRevBytes() ? 0x40 : 0;
        flag |= config[configurationIndex].getConfigurationFlags2().getbUseInvDataF() ? 0x80 : 0;

        outputBufferData[1] =  (byte) flag;

        outputBufferCommand[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEIDFRMTPARMS2;

        sendGetFeatureReport(outputBufferCommand);
        sendGetFeatureReport(outputBufferData);

    }

    //---------------
    //--- Block 4 ---
    //---------------
    //writes values of Eex1Config,
    private void writeConfigBlock4(int configurationIndex) throws SDKException {
        //Dirty EEx1Params Start

        byte[] outputBufferCommand = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] outputBufferData = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];

        outputBufferData[0] = (byte) config[configurationIndex].getEex1Config().getLeadParityX();
        outputBufferData[1] = (byte) config[configurationIndex].getEex1Config().getTrailParityX();
//        outputBufferData[2] = (byte) config[configurationIndex].getEex1Config().getiPad2();
//        outputBufferData[3] = (byte) config[configurationIndex].getEex1Config().getiPad3();
//        outputBufferData[4] = (byte) config[configurationIndex].getEex1Config().getiPad4();
//        outputBufferData[5] = (byte) config[configurationIndex].getEex1Config().getiPad5();
//        outputBufferData[6] = (byte) config[configurationIndex].getEex1Config().getiPad6();

        /*
       pcprox.cpp line number 1742

        outputBuffer2[5] &= ~0x02;

        outputBuffer2[5] |=*/

        outputBufferCommand[0] = HexConstants.b_SETFEATCMD_SLCTBLK | HexConstants.b_EEXPARMS1;
//
        sendGetFeatureReport(outputBufferCommand);
        sendGetFeatureReport(outputBufferData);
        //DIRTY bEEX1ParamsDirty end
    }

    //COMMMIT RAM TO EE
    private void writeToEE() throws SDKException {
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        outputBuffer[0] = HexConstants.b_SETFEATCMD_WRTEE  ;
        //This relates to dirty bit and optimizng the write config based on dirty bit.
        // Changes the static assignment of us once dirty bit optimization is in place.
        byte uc = (byte) 143; //TODO: will be implemented once flag optimisation is done

        outputBuffer[1] = uc;
        sendGetFeatureReport(outputBuffer);
    }

    /**
     * This sets the device configuration to the Factory Default values. It is
     * like a {@link Reader#WriteCfg(int)} call.
     * <p>Before returning to the caller, this function
     * calls ReadCfg() to reload the configuration information (which may have changed)
     * into the library memory.
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ResetFactoryDefaults();}
     * </pre>
     */
    public void ResetFactoryDefaults() throws SDKException {
        byte outputBuffer[] = new byte [HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte inputBuffer[];

        outputBuffer[0] = HexConstants.b_SETFEATCMD_SETDFLTS;

        inputBuffer = sendGetFeatureReport(outputBuffer);
        try {
            Thread.sleep(HexConstants.TIMETOCOMMIT);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int index = 0; index < GetMaxConfig(); index++) {
            ReadCfg(index);
        }

    }

    /**
     * Sets the BLE configuration of the reader.
     * @param level 0, 1, 2, 3 or 4
     * <ul><li>0:BLE off/125KHz-13.56MHz radios off</li>
     * <li>1:BLE off/125KHz-13.56MHz radios on</li>
     * <li>2:BLE on/125KHz-13.56MHz radios off</li>
     * <li>3:BLE on/125KHz-13.56MHz radios on</li>
     * <li>4:BLE off/125KHz-13.56MHz radios toggle</li>
     * @throws SDKException if there is a failure in USB communication or level is not between 0-4.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      readers[0].SetBLEConfiguration((byte)3);}
     * </pre>
     * @see Reader#GetBLEConfiguration()
     */
    public void SetBLEConfiguration (byte level) throws SDKException{

        if (0 <= level && level <= 4) {
            byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
            byte[] inputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];

            outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
            outputBuffer[1] = (byte) 0x87;
            outputBuffer[2] = level;

            inputBuffer = sendGetFeatureReport(outputBuffer);
        }
        else throw new SDKException("level must be between 0 and 4");
    }

    /**
     * Get the version of the library code. This does not communicate with any device.
     * It returns the string from the BuildConfig file.
     * The intended interpretation of the version name is Major.Minor.Build.Hotfix
     * @return Version of the library code.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      String libraryVersion = readers[0].getLibVersion();}
     * </pre>
     */
    public String  getLibVersion() {
        return  BuildConfig.VERSION_NAME;

    }

    /**
     * Gets the BLE configuration of the reader. It is applicable only for loon readers which support
     * bluetooth functionality.
     * @return level:
     * <ul><li>0 for BLE off/125KHz-13.56MHz radios off</li>
     * <li>1 for BLE off/125KHz-13.56MHz radios on</li>
     * <li>2 for BLE on/125KHz-13.56MHz radios off</li>
     * <li>3 for BLE on/125KHz-13.56MHz radios on</li>
     * <li>-1 for unsupported readers</li></ul>
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      int level = readers[0].GetBLEConfiguration();}
     * </pre>
     * @see Reader#SetBLEConfiguration(byte)
     */
    public int GetBLEConfiguration () throws SDKException{
        int level = -1;
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] inputBuffer;

        outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
        outputBuffer[1] = 0x07;

        inputBuffer = sendGetFeatureReport(outputBuffer);
        if((inputBuffer[2] & 0x80)!=0)
            level = (inputBuffer[2] & 0x03);
        return level;
    }

    /**
     * To check whether the BLE(Bluetooth) is present or not.
     * @return true if successful, false otherwise
     * @throws SDKException if there is a failure in USB communication.
     * @example
     * <pre>
     *     {@code private Reader[] readers = Enumerate.USBConnect(appContext);
     *      readers[0].ReadCfg(0);
     *      boolean _isBLEDevice = readers[0].IsBLEPresent();}
     * </pre>
     * @see Reader#GetBLEConfiguration()
     */
    public boolean IsBLEPresent()throws SDKException {
        boolean result = false;
        byte[] outputBuffer = new byte[HexConstants.FEATURE_REPORT_BUFFER_SIZE];
        byte[] inputBuffer;

        outputBuffer[0] = HexConstants.b_ALL_PROD_USB;
        outputBuffer[1] = 0x07;

        inputBuffer = sendGetFeatureReport(outputBuffer);
        if ((inputBuffer[2] & 0x80) != 0) {
            result = true;
        }
        return result;
    }

    private boolean SetDevTypeSrch(short iSrchType) throws SDKException{
        switch (iSrchType)
        {
            case HexConstants.PRXDEVTYP_USB:  // USB
            case HexConstants.PRXDEVTYP_SER:  // RS232
            case HexConstants.PRXDEVTYP_TCP:
            case HexConstants.PRXDEVTYP_ALL:
                _iCnctDevType = iSrchType;
                break;

            default: // all others illegal
                return false;
        }
        return true;
    }
}

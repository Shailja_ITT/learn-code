#include"stdafx.h"
#include"iostream"
//#include <bits/stdc++.h>

using namespace std;

#define max(value1, value2) (((value1) > (value2)) ? (value1) : (value2))

struct node
{
	int data;
	struct node *leftChild;
	struct node *rightChild;
};
class MonkWatchingFight{
public:
	node *root = NULL;
	struct node *loadValueToBeInserted(int value)
	{
		root = findLocationToInsertElementIntoTree(root, value);
		return root;
	}
	struct node *findLocationToInsertElementIntoTree(struct node *root, int value)
	{
		if (root == NULL)
			return (insertNewNode(value));
		else if (value < root->data)
			root->leftChild = findLocationToInsertElementIntoTree(root->leftChild, value);
		else if (value>root->data)
			root->rightChild = findLocationToInsertElementIntoTree(root->rightChild, value);

		return root;
	}

	struct node *insertNewNode(int value)
	{
		struct node *firstElementIsertionAsRoot = (struct node *)malloc(sizeof(struct node));
		firstElementIsertionAsRoot->data = value;
		firstElementIsertionAsRoot->leftChild = NULL;
		firstElementIsertionAsRoot->rightChild = NULL;
		return firstElementIsertionAsRoot;
	}
	int FindHeightOfBST(node *root){
		return(ProcessHeightOfBST(root));
	}
	int ProcessHeightOfBST(struct node *tree)
	{
		int l = 0, r = 0;
		if (tree == NULL)
			return 0;
		l = ProcessHeightOfBST(tree->leftChild);
		r = ProcessHeightOfBST(tree->rightChild);
		return max(l, r) + 1;
	}
};

int main()
{
	struct node *root = NULL;
	MonkWatchingFight BST;
	int noOfElements, elementIndex, element, heightOfBST;
	cin >> noOfElements;
	for (elementIndex = 0; elementIndex < noOfElements; elementIndex++)
	{
		cin >> element;
		root = BST.loadValueToBeInserted(element);
	}
	heightOfBST = BST.FindHeightOfBST(root);
	cout << heightOfBST << endl;
	return 0;
}


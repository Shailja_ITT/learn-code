#pragma once
#ifndef BST
# define BST

#define max(value1, value2) (((value1) > (value2)) ? (value1) : (value2))
struct node
{
	int data;
	struct node *leftChild;
	struct node *rightChild;
};

class MonkWatchingFight
{
public:
	node *root = NULL;
	struct node *loadValueToBeInserted(int value);
	struct node *findLocationToInsertElementIntoTree(struct node *root, int value);
	struct node *insertNewNode(int value);
	int FindHeightOfBST(node *root);
	int ProcessHeightOfBST(struct node *tree);
};

#endif
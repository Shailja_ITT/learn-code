
Synopsis :
----------

This is an android example to show how RFIDeas's SDK can be used for android
app development. This application can be used to list all rfidea's devices,
make them beep etc.

Prerequisites :
--------------

1. Android Studio 3.0.1 or above
2. pcProxAPI.aar

NOTE:
    1. To know, how you can install android studio on your platform please read
	   their official document.
    2. Minimum SDK version of android required is 12.

How to add pcProxAPI library as a dependency :
----------------------------------------------

1. Add the AAR file i.e. pcProxAPI.aar to your project:
	1. Click File > New > New Module.
	2. Click Import .JAR/.AAR Package then click Next.
	3. Enter the location of the compiled AAR file then click Finish.

2. Make sure the library is listed at the top of your settings.gradle file,
   as shown here for a library named "pcProxAPI":
   include ':app', ':pcProxAPI'

3. Open the app module's build.gradle file and add a new line to the
   dependencies block as shown in the following snippet:
   dependencies {
       compile project(":pcProxAPI")
   }

4. Click Sync Project with Gradle Files.

NOTE: To know more about, please read their official document.

How to get permission to access rfideas's devices in your application:
---------------------------------------------------

For this user need to edit AndroidManifest.xml and create resource file:

1. Edit the manifest file to add the following intent filter and associated meta-data:
<manifest ...>
    <uses-feature android:name="android.hardware.usb.host" />
    <uses-sdk android:minSdkVersion="12" />
    ...
    <application>
        <activity ...>
            ...
            <intent-filter>
                <action android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED" />
            </intent-filter>

            <meta-data android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED"
                android:resource="@xml/usb_device_filter" />
	        ...
        </activity>
    </application>
</manifest>

2. Create a file usb_device_filter.xml at path res/xml/ and replace/copy the below code:
<?xml version="1.0" encoding="utf-8"?>

<resources>
    <usb-device vendor-id="3111" product-id="15134" class="239" subclass="2" protocol="1" />
    <usb-device vendor-id="3111" product-id="15354" class="0" subclass="0" protocol="0" />
</resources>


How to make :
-------------

Click Build > Make Project.

Note :
After successful build, you will find the .apk file at path /app/build/outputs/apk/<build variant>/

Frequently Asked Questions(FAQ) :
--------------------------------

1. Why my virtual keyboard is not displaying after connecting the reader?

	It is happening because your android device consider the rfideas reader as
	a physical keyboard. You have to explicitly turn on the "Show virtual keyboard"
	if a physical keyboard is active from settings.

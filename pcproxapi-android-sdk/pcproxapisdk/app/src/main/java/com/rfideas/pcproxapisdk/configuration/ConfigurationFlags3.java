package com.rfideas.pcproxapisdk.configuration;

/**
 * Contains methods to alter variables defined under GetFlags3() API
 */
public class ConfigurationFlags3 {
    private boolean bUseNumKP;     // Euro KB flag
    private boolean bSndSFON;      // Split format ON = 1, old combined scheme = 0
    private boolean bSndSFFC;      // 0 = FAC Decimal, 1 = FAC Hex
    private boolean bSndSFID;      // 0 = ID Decimal, 1 = ID Hex
    private boolean bPrxProEm;     // Use ProxPro emulation
    private boolean bUse64Bit;     // 0 = 32-bit, 1 = 64-bit Display Math
    private boolean bNotBootDev;   // USB Enum: 0=BootDevice, 1=NOTBootDevice
    private boolean bLowerCaseHex; // Alpha hex output displayed as lower case


    public boolean getbUseNumKP() {
        return bUseNumKP;
    }

    public void setbUseNumKP(boolean bUseNumKP) {
        this.bUseNumKP = bUseNumKP;
    }

    public boolean getbSndSFON() {
        return bSndSFON;
    }

    public void setbSndSFON(boolean bSndSFON) {
        this.bSndSFON = bSndSFON;
    }

    public boolean getbSndSFFC() {
        return bSndSFFC;
    }

    public void setbSndSFFC(boolean bSndSFFC) {
        this.bSndSFFC = bSndSFFC;
    }

    public boolean getbSndSFID() {
        return bSndSFID;
    }

    public void setbSndSFID(boolean bSndSFID) {
        this.bSndSFID = bSndSFID;
    }

    public boolean getbPrxProEm() {
        return bPrxProEm;
    }

    public void setbPrxProEm(boolean bPrxProEm) {
        this.bPrxProEm = bPrxProEm;
    }

    public boolean getbUse64Bit() {
        return bUse64Bit;
    }

    public void setbUse64Bit(boolean bUse64Bit) {
        this.bUse64Bit = bUse64Bit;
    }

    public boolean getbNotBootDev() {
        return bNotBootDev;
    }

    public void setbNotBootDev(boolean bNotBootDev) {
        this.bNotBootDev = bNotBootDev;
    }

    public boolean getbLowerCaseHex() {
        return bLowerCaseHex;
    }

    public void setbLowerCaseHex(boolean bLowerCaseHex) {
        this.bLowerCaseHex = bLowerCaseHex;
    }
}

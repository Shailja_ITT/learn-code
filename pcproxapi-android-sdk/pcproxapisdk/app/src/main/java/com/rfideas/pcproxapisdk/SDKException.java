package com.rfideas.pcproxapisdk;

/**
 * Custom exception handling all the Exceptions in API.
 * It throws suitable messages wherever required.
 * <p>
 * Developer must handle this exception while calling the APIs.
 * <p>Some APIs which uses Thread.sleep() also throws InterruptedException
 */
public class SDKException extends Exception {
    public SDKException(String message){
        super(message);
    }
}

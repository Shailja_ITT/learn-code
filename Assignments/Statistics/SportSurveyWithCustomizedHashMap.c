#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct nameofPerson
{
	int name;
	struct nameofPerson* next;
};

struct sportLikes
{
	char sportName[21];
	int count;
	struct sportLikes * next;
};

struct nameofPerson ** hashname;
struct sportLikes **hashsportName;
int size;
unsigned long GetHashcode(unsigned char *str)
{
	unsigned long hashSize = 5381;
	int c;

	while (c = *str++)
		hashSize = ((hashSize << 5) + hashSize) + c; /* hashSize * 33 + c */

	return hashSize;
}

void RecordingTheSurvey(int noOfPeople)
{
	char nameOfPerson[21], nameOfSport[21], nameOfFavSport[21]; 
	int likesForFootball = 0;
	int noOfLikes, maxLikes = 0;
	while (noOfPeople--)
	{
		scanf("%s %s", nameOfPerson, nameOfSport);
		if (!IsPresentAlreadyPresent(nameOfPerson))
		{
			noOfLikes = CountOfPeopleLikingtheSport(nameOfSport);
			if (noOfLikes>maxLikes || (noOfLikes == maxLikes && (strcmp(nameOfSport, nameOfFavSport)<0)))
			{
				maxLikes = noOfLikes;
				strcpy(nameOfFavSport, nameOfSport);
			}
			if (strcmp(nameOfSport, "football") == 0)
				likesForFootball++;
		}
	}
	printf("%s\n%d", nameOfFavSport, likesForFootball);
}

int IsPresentAlreadyPresent(char *nameOfPerson)
{
	unsigned long hashcodeSize = GetHashcode(nameOfPerson);
	int pos = hashcodeSize%size;
	struct nameofPerson* currentName= hashname[pos];
	while (currentName)
	{
		if (currentName->name == hashcodeSize)
			return 1;
		currentName = currentName->next;
	}
	struct nameofPerson* head = (struct nameofPerson*)malloc(sizeof(struct nameofPerson));
	head->name = hashcodeSize;
	head->next = hashname[pos];
	hashname[pos] = head;
	return 0;
}
int CountOfPeopleLikingtheSport(char* nameOfSport)
{
	unsigned long hashcodeSize = GetHashcode(nameOfSport);
	int pos = hashcodeSize%size;
	struct sportLikes* currentSport = hashsportName[pos];
	while (currentSport)
	{
		if (strcmp(nameOfSport, currentSport->sportName) == 0)
		{
			currentSport->count++;
			return currentSport->count;
		}
		currentSport = currentSport->next;
	}
	struct sportLikes* head = (struct sportLikes*)malloc(sizeof(struct sportLikes));
	strcpy(head->sportName, nameOfSport);
	head->count = 1;
	head->next = hashsportName[pos];
	hashsportName[pos] = head;
	return 1;
}

int main()
{
	int noOfPeople;
	size = 1000000;
	scanf("%d", &noOfPeople);
	hashname = (struct nameofPerson**)malloc(size*sizeof(struct nameofPerson*));
	hashsportName = (struct sportLikes**)malloc(size*sizeof(struct sportLikes*));
	RecordingTheSurvey(noOfPeople);

	return 0;
}


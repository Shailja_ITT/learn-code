package com.rfideas.automatedtesting;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.rfideas.pcproxapisdk.Enumerate;
import com.rfideas.pcproxapisdk.Reader;
import com.rfideas.pcproxapisdk.SDKException;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags2;
import com.rfideas.pcproxapisdk.configuration.IdBitCounts;
import com.rfideas.pcproxapisdk.configuration.LEDControl;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private Context appContext = InstrumentationRegistry.getTargetContext();
    private Reader[] readers = Enumerate.USBConnect(appContext);

    public ExampleInstrumentedTest() throws SDKException {
    }


    @Test
    public void idBitCountTest() throws SDKException, InterruptedException {

        readers[0].ReadCfg(0);
        IdBitCounts idBitCountsWritten = readers[0].GetIDBitCnts(0);
        idBitCountsWritten.setiLeadParityBitCnt((short) 2);
        idBitCountsWritten.setiTrailParityBitCnt((short) 4);
        idBitCountsWritten.setiIDBitCnt((short) 20);
        idBitCountsWritten.setiTotalBitCnt((short) 40);
        readers[0].WriteCfg(0);
        readers[0].ReadCfg(0);
        idBitCountsWritten.setiLeadParityBitCnt((short) 3);

        assertEquals(idBitCountsWritten.getiLeadParityBitCnt(), readers[0].GetIDBitCnts(0).getiLeadParityBitCnt());
    }

    @Test
    public void getIdBitCountsTest() throws SDKException {

        readers[0].ReadCfg(0);
        assertEquals(2, readers[0].GetIDBitCnts(0).getiLeadParityBitCnt());
        assertEquals(4, readers[0].GetIDBitCnts(0).getiTrailParityBitCnt());
        assertEquals(20, readers[0].GetIDBitCnts(0).getiIDBitCnt());
        assertEquals(40, readers[0].GetIDBitCnts(0).getiTotalBitCnt());


    }


    @Test
    public void setIdBitCountTest() throws SDKException, InterruptedException {

        readers[0].ReadCfg(0);
        IdBitCounts idBitCountsWritten = readers[0].GetIDBitCnts(0);
        idBitCountsWritten.setiLeadParityBitCnt((short) 2);
        idBitCountsWritten.setiTrailParityBitCnt((short) 4);
        idBitCountsWritten.setiIDBitCnt((short) 20);
        idBitCountsWritten.setiTotalBitCnt((short) 40);
        readers[0].WriteCfg(0);
        readers[0].ReadCfg(0);
        assertEquals(2, readers[0].GetIDBitCnts(0).getiLeadParityBitCnt());
        assertEquals(4, readers[0].GetIDBitCnts(0).getiTrailParityBitCnt());
        assertEquals(20,readers[0].GetIDBitCnts(0).getiIDBitCnt());
        assertEquals(40,readers[0].GetIDBitCnts(0).getiTotalBitCnt());

    }


    @Test
    public void getBeeperVolumeTest() throws SDKException {

        assertEquals(3, readers[0].GetBeeperVolume());

    }

    @Test
    public void setBeeperVolumeTest() throws SDKException {

        final byte beeperVolume = 2;
        readers[0].SetBeeperVolume(beeperVolume);

        assertEquals(beeperVolume, readers[0].GetBeeperVolume());

    }
    @Test
    public void SetBLEValueChecksTest() throws SDKException{
        readers[0].SetBeeperVolume((byte)6);
        assertEquals(6,readers[0].GetBeeperVolume());
    }

    @Test
    public void BLETest() throws SDKException{
        readers[0].SetBLEConfiguration((byte)3);
        assertEquals(3,readers[0].GetBLEConfiguration());
    }


    @Test
    public void maxConfigTest() {
        assertEquals(3, readers[0].GetMaxConfig());

    }

    @Test
    public void GetPartNumberStringTest() throws SDKException {

        assertEquals("RDR-305x1AxU", readers[0].getPartNumberString());
    }

    @Test
    public void BeepNowTest() throws SDKException {
        readers[0].BeepNow((byte)4, false);
    }

    @Test
    public void setFlagsTest() throws SDKException, InterruptedException{

        readers[0].ReadCfg(0);
        ConfigurationFlags configurationFlags = readers[0].GetFlags(0);
        configurationFlags.setbFixLenDsp(true);
        configurationFlags.setbFrcBitCntEx(true);
        configurationFlags.setbHaltKBSnd(true);
        configurationFlags.setbNoUseELChar(true);
        configurationFlags.setbSndFac(true);
        configurationFlags.setbSndOnRx(true);
        configurationFlags.setbStripFac(true);
        configurationFlags.setbUseDelFac2Id(true);
        readers[0].WriteCfg(0);
        readers[0].ReadCfg(0);
        assertEquals(true, readers[0].GetFlags(0).getbFixLenDsp());
        assertEquals(true, readers[0].GetFlags(0).getbFrcBitCntEx());
        assertEquals(true, readers[0].GetFlags(0).getbHaltKBSnd());
        assertEquals(true, readers[0].GetFlags(0).getbNoUseELChar());
        assertEquals(true, readers[0].GetFlags(0).getbSndFac());
        assertEquals(true, readers[0].GetFlags(0).getbSndOnRx());
        assertEquals(true, readers[0].GetFlags(0).getbStripFac());
        assertEquals(true, readers[0].GetFlags(0).getbUseDelFac2Id());
    }

    @Test
    public void Flags2Test() throws SDKException, InterruptedException {
        readers[0].ReadCfg(0);
        ConfigurationFlags2 configurationFlags2 = readers[0].GetFlags2(0);
        configurationFlags2.setbUseLeadChrs(true);
        configurationFlags2.setbDspHex(false);
        configurationFlags2.setbWiegInvData(true);
        configurationFlags2.setbBeepID(true);
        configurationFlags2.setbRevWiegBits(false);
        configurationFlags2.setbRevBytes(true);
        configurationFlags2.setbUseInvDataF(false);
        readers[0].WriteCfg(0);
        readers[0].ReadCfg(0);
        assertEquals(true, readers[0].GetFlags2(0).getbUseLeadChrs());
        assertEquals(false, readers[0].GetFlags2(0).getbDspHex());
        assertEquals(true, readers[0].GetFlags2(0).getbWiegInvData());
       assertEquals(true, readers[0].GetFlags2(0).getbBeepID());
        assertEquals(false, readers[0].GetFlags2(0).getbRevWiegBits());
        assertEquals(true, readers[0].GetFlags2(0).getbRevBytes());
        assertEquals(false, readers[0].GetFlags2(0).getbUseInvDataF());
    }

    @Test
    public void ResetFactoryDefaultsTest() throws SDKException{
        readers[0].ReadCfg(0);
        readers[0].ResetFactoryDefaults();
        readers[0].ReadCfg(0);
        assertEquals(61188,readers[0].GetCardType(0));
        assertEquals(1, readers[0].GetIDBitCnts(0).getiLeadParityBitCnt());
        assertEquals(1, readers[0].GetIDBitCnts(0).getiTrailParityBitCnt());
        assertEquals(16, readers[0].GetIDBitCnts(0).getiIDBitCnt());
        assertEquals(26, readers[0].GetIDBitCnts(0).getiTotalBitCnt());

    }



    //61188 HID prox 608 compatible
    @Test
    public void SetCardTypePriorityTest() throws SDKException, InterruptedException {
        readers[0].SetCardTypePriority(0,64514,1);
        readers[0].SetCardTypePriority(1,64514,0);
        readers[0].WriteCfg(0);
        readers[0].ReadCfg(0);
        readers[0].WriteCfg(1);
        readers[0].ReadCfg(1);
        assertEquals(64514,readers[0].GetCardType(0));
        assertEquals(64514,readers[0].GetCardType(1));
        assertEquals(1,readers[0].GetCardPriority(0));
        assertEquals(0,readers[0].GetCardPriority(1));

    }

    @Test
    public void FWFilenameTest() throws SDKException{
        assertEquals("LNC160700UBX700", readers[0].GetFWFilename());
    }

    @Test
    public void FullFWVersionTest() throws SDKException{
        assertEquals("16.07.0-7715", readers[0].GetFullFWVersion());
    }

    @Test
    public void LUIDTest() throws SDKException, InterruptedException {
        readers[0].ReadCfg(0);
        readers[0].setLuid(11);
        readers[0].WriteCfg(0);
        readers[0].ReadCfg(0);
        assertEquals(11, readers[0].getLuid());
    }

    @Test
    public void LibVersionTest() {
        assertEquals("0.0.2.0",readers[0].getLibVersion());
    }

    @Test
    public void LEDTest() throws SDKException, InterruptedException {
        readers[0].ReadCfg(0);
        readers[0].GetLEDControl(0).setbAppCtrlsLED(true);
        readers[0].GetLEDControl(0).setiGrnLEDState(true);
        readers[0].GetLEDControl(0).setiRedLEDState(false);
        readers[0].WriteCfg(0);
        readers[0].ReadCfg(0);
        assertEquals(true,readers[0].GetLEDControl(0).getbAppCtrlsLED());
        assertEquals(true,readers[0].GetLEDControl(0).getiGrnLEDState());
        assertEquals(false,readers[0].GetLEDControl(0).getiRedLEDState());

    }

}

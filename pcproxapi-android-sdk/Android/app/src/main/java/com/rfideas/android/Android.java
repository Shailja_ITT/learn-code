package com.rfideas.android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import com.rfideas.pcproxapisdk.Enumerate;
import com.rfideas.pcproxapisdk.SDKException;
import com.rfideas.pcproxapisdk.Reader;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags;
import android.view.View.OnClickListener;


public class Android extends AppCompatActivity implements OnClickListener{

    private Reader reader[];
    Button connect;
    Button disconnect;
    Button beepNow;
    Button getActiveId;
    TextView sdkOutput;
    TextView readerPartNumber;
    TextView maxConfigurationNumber;
    CheckBox disableKeystrokes;

    private static final String TAG = "Android";

    int readerNo = 0; // To access the first connected reader.
    int configurationIndex = 0; //  To access the first configuration of the reader in use.


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android);

        //defining all the architectural components from the UI
        connect = (Button) findViewById(R.id.connectButton);
        disconnect = (Button) findViewById(R.id.disconnectButton);
        beepNow = (Button) findViewById(R.id.beepnowButton);
        getActiveId = (Button) findViewById(R.id.getActiveIdButton);
        sdkOutput = (TextView) findViewById(R.id.sdkOutputTextView);
        readerPartNumber = (TextView)findViewById(R.id.partNumberText);
        disableKeystrokes = (CheckBox) findViewById(R.id.disableKeystrokesCheckBox);
        maxConfigurationNumber = (TextView)findViewById(R.id.displayMaxConfigurationNumber);

        //activating functionality on clicking the component
        connect.setOnClickListener(this);
        beepNow.setOnClickListener(this);
        disconnect.setOnClickListener(this);
        getActiveId.setOnClickListener(this);
        disableKeystrokes.setOnClickListener(this);
        checkKeystrokesForUI();
    }

    @Override
    public void onClick(View view) {
        try
        {
            byte beepNumber = 2; //for number of beeps to be passed in BeepNow()
            switch (view.getId())
            {
                case R.id.connectButton:
                    connectTheDevice();
                    break;

                case R.id.beepnowButton:
                    reader[readerNo].BeepNow(beepNumber,Boolean.TRUE);
                    break;

                case R.id.getActiveIdButton:
                    sdkOutput.setText(null);
                    String buffer = getActiveIdOutput();
                    sdkOutput.setText(buffer);
                    break;

                case R.id.disconnectButton:
                    disconnectTheDevice();
                    break;

                default:
                    break;
            }
        }
        catch (SDKException ive) {
            Log.e(TAG, "Invalid value of input or Exception in USB Communication", ive);
        }
        catch(Exception exception) {
            Log.e(TAG, "Exception occured", exception);
        }

    }

    public void connectTheDevice() throws SDKException {
        reader = Enumerate.USBConnect(getApplicationContext());

        if (reader == null) {
            Toast.makeText(this, "No Device Connected", Toast.LENGTH_LONG).show();
            return;
        }
        String partNumberString = reader[readerNo].getPartNumberString();
        readerPartNumber.setText(partNumberString);

        //display the maximum configuration possessed by a reader
        Integer maximumConfiguration = reader[readerNo].GetMaxConfig() + 1;
        maxConfigurationNumber.setText(maximumConfiguration + "");

        //A call to readCfg reads all the values  of the specified configuration and stores them in local memory.
        // Developer must call this API before attempting to access/alter any settings.
        reader[readerNo].ReadCfg(configurationIndex);


        //to have disableKeystrokes checkbox checked in UI if internally haltKBSend is enabled i.e taking the state previously saved
        final ConfigurationFlags configurationFlags = reader[readerNo].GetFlags(configurationIndex);
        if (configurationFlags.getbHaltKBSnd())
            disableKeystrokes.setChecked(true);
        else
            disableKeystrokes.setChecked(false);

    }

    public void disconnectTheDevice() {
        Enumerate.USBDisconnect();
        readerPartNumber.setText(null);
        maxConfigurationNumber.setText(null);
        sdkOutput.setText(null);
    }

    public void checkKeystrokesForUI(){
        //when disableKeystrokes checkbox is checked or unchecked is done in UI then the change is saved in the reader
        disableKeystrokes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                //Checking if reader is not null so that when no reader is connected, it should not try to change any of the Configuration Flags in savingTheConfiguration method
                if (reader != null)
                {
                    try {
                        savingTheConfiguration();
                    }
                    catch (SDKException ive) {
                        Log.e(TAG, "Invalid value of input or Exception in USB Communication", ive);
                    }
                    catch(Exception exception) {
                        Log.e(TAG, "Exception ", exception);
                    }
                }
            }
        });
    }


    public void savingTheConfiguration() throws SDKException, InterruptedException {
        boolean haltKBSnd;

        //check whether disableKeystrokes checbox is checked so as to save setbHaltKBSnd
        if(disableKeystrokes.isChecked())
            haltKBSnd = true;
        else
            haltKBSnd = false;

        //Pulling the ConfigurationFlags object and changing the value of haltKBSnd in the object and writing the changed value using WriteCfg
        ConfigurationFlags configurationFlags = reader[readerNo].GetFlags(configurationIndex);
        configurationFlags.setbHaltKBSnd(haltKBSnd);
        reader[readerNo].SetFlags(configurationFlags,configurationIndex);
        reader[readerNo].WriteCfg(configurationIndex);
    }



    public String getActiveIdOutput()throws SDKException {
        //Added sleep as mentioned in the API documentation
        try{
            Thread.sleep(250);
        }
        catch(InterruptedException ie){
            Log.e(TAG, "sleep interrupted", ie);
        }
        short bits;
        short size = 32;
        int[] buf = new int[size];
        bits = reader[readerNo].GetActiveID(buf, size);
        if (bits == 0) {
            Toast.makeText(this, "No id found, Please put card on the reader", Toast.LENGTH_LONG).show();
        } else {
            String buffer = "";
            String temp = "";
            int bytesToRead = (bits + 7) / 8;
            if (bytesToRead < 8) {
                bytesToRead = 8;
            }
            for (int i = 0; i < bytesToRead; i++) {
                temp = Integer.toHexString(buf[i]);
                buffer = buffer.concat(temp);
                buffer = buffer.concat(" ");
            }
            Toast.makeText(this, bits + " Bits: " + buffer.toUpperCase(), Toast.LENGTH_LONG).show();
            return buffer;
        }
        return null;
    }
}
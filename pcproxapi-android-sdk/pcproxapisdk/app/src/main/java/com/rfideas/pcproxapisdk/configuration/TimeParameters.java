package com.rfideas.pcproxapisdk.configuration;


import com.rfideas.pcproxapisdk.SDKException;

public class TimeParameters {

    private short iBitStrmTO;   // Wiegand read T/O after this msec time (4ms)
    private short iIDHoldTO;    // Card ID valid for this msec time (48ms)
    private short iIDLockOutTm; // Squelch repetitive reader reports (usually > 1000) in msec (48msec gran.)
    private short iUSBKeyPrsTm; // Sets USB inter-key 'Press' time in 4ms units
    private short iUSBKeyRlsTm; // Sets USB inter-key 'Release' time in 4ms units
    private short ExFeatures01; // Extended Features (big parity Azery ext precision)
    private short iPad6;        // Spare Unused
    private short iTPCfgFlg3;   // Bit mapped tp Flags3 -- Use Flags3 structure instead

    public short getiBitStrmTO() {
        return iBitStrmTO;
    }

    public void setiBitStrmTO(short iBitStrmTO) throws SDKException {
        if (iBitStrmTO < 0 || iBitStrmTO > 1020) {
            throw new SDKException("Bit Stream Time Out must be in range from 0 to 1020");
        }
        else {
            this.iBitStrmTO = iBitStrmTO;
        }
    }

    public short getiIDHoldTO() {
        return iIDHoldTO;
    }

    public void setiIDHoldTO(short iIDHoldTO) throws SDKException {

        if (iIDHoldTO < 0 || iIDHoldTO > 12750) {
            throw new SDKException("Id Hold Time Out must be in range from 0 to 12750");
        }
        else {
            this.iIDHoldTO = iIDHoldTO;
        }
    }

    public short getiIDLockOutTm() {
        return iIDLockOutTm;
    }

    public void setiIDLockOutTm(short iIDLockOutTm) throws SDKException {
        if (iIDLockOutTm < 0 || iIDLockOutTm > 12750) {
            throw new SDKException("Id Lock out time must be in range from 0 to 12750");
        }
        else {
            this.iIDLockOutTm = iIDLockOutTm;
        }
    }

    public short getiUSBKeyPrsTm() {
        return iUSBKeyPrsTm;
    }

    public void setiUSBKeyPrsTm(short iUSBKeyPrsTm) throws SDKException {
        if (iUSBKeyPrsTm < 0 || iUSBKeyPrsTm > 1020) {
            throw new SDKException("USB Key press time must be in range from 0 to 1020");
        }
        else {
            this.iUSBKeyPrsTm = iUSBKeyPrsTm;
        }
    }

    public short getiUSBKeyRlsTm() {
        return iUSBKeyRlsTm;
    }

    public void setiUSBKeyRlsTm(short iUSBKeyRlsTm) throws SDKException {
        if (iUSBKeyRlsTm < 0 || iUSBKeyRlsTm > 1020) {
            throw new SDKException("USB Key release time must be in range from 0 to 1020");
        }
        this.iUSBKeyRlsTm = iUSBKeyRlsTm;
    }

    public short getExFeatures01() {
        return ExFeatures01;
    }

    public void setExFeatures01(short exFeatures01) {
        ExFeatures01 = exFeatures01;
    }

    public short getiTPCfgFlg3() {
        return iTPCfgFlg3;
    }

    public void setiTPCfgFlg3(short iTPCfgFlg3) {
        this.iTPCfgFlg3 = iTPCfgFlg3;
    }
}

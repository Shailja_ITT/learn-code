package com.rfideas.pcproxapisdk.configuration;


public class LEDControl {
    private boolean bAppCtrlsLED; // [pcProx 0=Auto, 1=App], [pcSwipe 1=Amber Transition]
    private boolean iRedLEDState; // 0 == Off, 1 == On

    public boolean getbAppCtrlsLED() {
        return bAppCtrlsLED;
    }

    public void setbAppCtrlsLED(boolean bAppCtrlsLED) {
        this.bAppCtrlsLED = bAppCtrlsLED;
    }

    public boolean getiRedLEDState() {
        return iRedLEDState;
    }

    public void setiRedLEDState(boolean iRedLEDState) {
        this.iRedLEDState = iRedLEDState;
    }

    public boolean getiGrnLEDState() {
        return iGrnLEDState;
    }

    public void setiGrnLEDState(boolean iGrnLEDState) {
        this.iGrnLEDState = iGrnLEDState;
    }

    private boolean iGrnLEDState; // 0 == Off, 1 == On
}

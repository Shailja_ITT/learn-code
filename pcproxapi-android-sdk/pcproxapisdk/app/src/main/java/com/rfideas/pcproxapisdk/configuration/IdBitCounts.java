package com.rfideas.pcproxapisdk.configuration;


import com.rfideas.pcproxapisdk.SDKException;

public class IdBitCounts {

    private short iLeadParityBitCnt;  // Wiegand Leading Parity bit count to be stripped
    private short iTrailParityBitCnt; // Wiegand Trailing Parity bit count to be stripped
    private short iIDBitCnt;          // If bStripFac, determines bit count of ID and FAC
    private short iTotalBitCnt;       // If bFrcBitCntEx, card read (including parity) must match this
    private short iPad4;
    private short iPad5;
    private short iPad6;
    private short iPad7;

    public short getiLeadParityBitCnt() {
        return iLeadParityBitCnt;
    }

    public void setiLeadParityBitCnt(short iLeadParityBitCnt) throws SDKException {
        //Idealy it should validate values after checking whether the reader is single config or not
        //TODO: Improve implementation in next release.
        if(iLeadParityBitCnt < 0 || iLeadParityBitCnt > 142) {
            throw new SDKException("LP must be between 0 and 142");
        }
        else {
            this.iLeadParityBitCnt = iLeadParityBitCnt;
        }
    }

    public short getiTrailParityBitCnt() {
        return iTrailParityBitCnt;
    }

    public void setiTrailParityBitCnt(short iTrailParityBitCnt) throws SDKException {
        if(iTrailParityBitCnt < 0 || iTrailParityBitCnt > 142) {
            throw new SDKException("TP must be between 0 and 142");
        }
        else {
            this.iTrailParityBitCnt = iTrailParityBitCnt;
        }
    }

    public short getiIDBitCnt() {
        return iIDBitCnt;
    }

    public void setiIDBitCnt(short iIDBitCnt) throws SDKException {
        if(iIDBitCnt < 1 || iIDBitCnt > 255) {
            throw new SDKException("idBitCounts must be between 1 and 255");
        }
        else {
            this.iIDBitCnt = iIDBitCnt;
        }
    }

    public short getiTotalBitCnt() {
        return iTotalBitCnt;
    }

    public void setiTotalBitCnt(short iTotalBitCnt) throws SDKException {
        if(iTotalBitCnt < 26 || iTotalBitCnt > 255) {
            throw new SDKException("Total Bit counts must be between 26 and 255");
        }
        else {
            this.iTotalBitCnt = iTotalBitCnt;
        }
    }
}

package com.rfideas.pcproxapisdk.configuration;

public class IdDisplayParameters3 {
    public short getiTrailChrCnt() {
        return iTrailChrCnt;
    }

    public void setiTrailChrCnt(short iTrailChrCnt) {
        this.iTrailChrCnt = iTrailChrCnt;
    }

    public short getiTrailChr0() {
        return iTrailChr0;
    }

    public void setiTrailChr0(short iTrailChr0) {
        this.iTrailChr0 = iTrailChr0;
    }

    public short getiTrailChr1() {
        return iTrailChr1;
    }

    public void setiTrailChr1(short iTrailChr1) {
        this.iTrailChr1 = iTrailChr1;
    }

    public short getiTrailChr2() {
        return iTrailChr2;
    }

    public void setiTrailChr2(short iTrailChr2) {
        this.iTrailChr2 = iTrailChr2;
    }

    private short iTrailChrCnt; // This contains the trail char count (<=3)
    private short iTrailChr0;   // These trail characters are filled in (up to 3)
    private short iTrailChr1;   // NOTE: LeadChrCnt + TrailCheCnt <= 3
    private short iTrailChr2;
}

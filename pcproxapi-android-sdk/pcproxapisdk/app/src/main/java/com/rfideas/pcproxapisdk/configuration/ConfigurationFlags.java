package com.rfideas.pcproxapisdk.configuration;

/**
 * Contains methods to alter variables defined under GetFlags() API
 */
public class ConfigurationFlags {

    private boolean bFixLenDsp;     // Send as fixed length with leading zeros as needed
    private boolean bFrcBitCntEx;   // Force Rx'd bit count to be exact to be valid
    private boolean bStripFac;      // Strip the FAC from the ID (not discarded)
    private boolean bSndFac;        // Send the FAC (if stripped from data)
    private boolean bUseDelFac2Id;  // Put a delimiter between FAC and ID on send
    private boolean bNoUseELChar;   // Don't use a EndLine char on send (default to ENTER)
    private boolean bSndOnRx;       // Send valid ID as soon as it is received (iIDLockOutTm timer not used)
    private boolean bHaltKBSnd;     // Don't Send keys to USB (Get ID mechanism)

    public boolean getbFixLenDsp() { return bFixLenDsp; }

    public void setbFixLenDsp(boolean bFixLenDsp) {
        this.bFixLenDsp = bFixLenDsp;
    }

    public boolean getbFrcBitCntEx() {
        return bFrcBitCntEx;
    }

    public void setbFrcBitCntEx(boolean bFrcBitCntEx) {
        this.bFrcBitCntEx = bFrcBitCntEx;
    }

    public boolean getbStripFac() {
        return bStripFac;
    }

    public void setbStripFac(boolean bStripFac) {
        this.bStripFac = bStripFac;
    }

    public boolean getbSndFac() {
        return bSndFac;
    }

    public void setbSndFac(boolean bSndFac) {
        this.bSndFac = bSndFac;
    }

    public boolean getbUseDelFac2Id() {
        return bUseDelFac2Id;
    }

    public void setbUseDelFac2Id(boolean bUseDelFac2Id) {
        this.bUseDelFac2Id = bUseDelFac2Id;
    }

    public boolean getbNoUseELChar() {
        return bNoUseELChar;
    }

    public void setbNoUseELChar(boolean bNoUseELChar) {
        this.bNoUseELChar = bNoUseELChar;
    }

    public boolean getbSndOnRx() {
        return bSndOnRx;
    }

    public void setbSndOnRx(boolean bSndOnRx) {
        this.bSndOnRx = bSndOnRx;
    }

    public boolean getbHaltKBSnd() {
        return bHaltKBSnd;
    }

    public void setbHaltKBSnd(boolean bHaltKBSnd) {
        this.bHaltKBSnd = bHaltKBSnd;
    }
}

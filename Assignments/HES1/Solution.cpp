#include <iostream>
using namespace std;
class Football
{
private:
	int testcases;
	int numberOfPasses;
	int id1, id2;
public:
    //to take the number of testcases
	void PassingTestcases(){
	
		cin>>testcases;//taking number of test cases as input 
   		for(int i=0; i<testcases;i++) 
		{
			PassingPasses();
		}
	}
	
	//to take the number of passes
	void PassingPasses()
	{
		cin>>numberOfPasses;  //taking number of Passes as input from users
		cin>>id1; //taking input ID of first player 
    	id2=id1;
    	for(int j=0;j<numberOfPasses;j++)
		{
            char Pass_id;
        	cin>>Pass_id; //selecting the type of pass
        	if(Pass_id == 'P')
			{
            	int next_id;
        		cin>>next_id;
        		id1 = id2;
        		id2 = next_id;
        	}
        	else
			{
            	id1 = id1+id2;
            	id2 = id1-id2;
            	id1 = id1-id2;
        	}
   		}
    		cout<<"\nPlayer "<<id2<<endl;
	}
};

int main() 
{
	Football f;//object of class football
	f.PassingTestcases();
    return 0;
}
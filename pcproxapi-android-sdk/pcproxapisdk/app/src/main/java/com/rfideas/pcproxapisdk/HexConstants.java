package com.rfideas.pcproxapisdk;


public class HexConstants {

    public static final int VENDOR_ID = 3111;           //0C27
    public static final int PLUS_PRODUCT_ID = 15354;    //3BFA
    public static final int BLE_PRODUCT_ID = 15134;    //3B1E

    public static final int PRXDEVTYP_ALL = -1;
    public static final int PRXDEVTYP_USB = 0;
    public static final int PRXDEVTYP_SER = 1;
    public static final int PRXDEVTYP_TCP = 2;

    public static final int FEATURE_REPORT_BUFFER_SIZE = 8;

    public static final int MAX_PRODUCT_NAME_SIZE = 24;

    public static final int MAX_PLUS_CONFIG = 10;

    public static final byte b_ALL_PROD_USB = (byte) (0x80 | 0x0C);

    public static final byte b_ALL_PROD_SUB_GETSTRING = (byte) 0x04;

    public static final byte b_ALL_PROD_SUB_GET_BEEPER_VOLUME = (byte) 0x06;

    public static final byte b_ALL_PROD_SUB_SET_BEEPER_VOLUME = (byte) 0x86;

    public static final byte b_ALL_PROD_SUB_BEEP_NOW = (byte) 0x03;

    public static final byte b_ALL_PROD_SUB_GET_PART_NUMBER = (byte) 0x01;

    public static final byte b_SETFEATCMD_COMBO = (byte) 0x089;

    public static final byte b_SUBCMDREAD_COMBO = (byte) 0x001;     // Read prox+ card type priority etc Dec->PC

    public static final byte b_SUBCMDWRITE_COMBO = (byte) 0x081;   // write prox+ card type priority etc PC->Dev

    public static final String ACTION_USB_PERMISSION =
            "com.example.company.app.testhid.USB_PERMISSION";

    public static final int REQUEST_GET_REPORT = 0x01;

    public static final int REQUEST_SET_REPORT = 0x09;

    public static final int REPORT_TYPE_FEATURE = 0x0300;

    public static final int REPORT_TYPE_INPUT = 0x0100;

    public static final int REPORT_TYPE_OUTPUT = 0x0200;

    public static final byte b_SETFEATCMD_SLCTBLK = (byte) (0x80 | 0x00);   // select next data Xfr for both normal & extended

    public static final byte b_SETFEATCMD_SETDFLTS = (byte) (0x80 | 0x20);  //restore Factory Default settings

    public static final byte b_EETMPARMS = (byte) 0x01; // IN/OUT Timing parameters

    public static final byte b_EEIDFRMTPARMS2 = (byte) 0x02;    // IN/OUT Card Format / Display parameters Extended

    public static final byte b_EEXPARMS1 = (byte) 0x03; // IN/OUT Extended parameters 1st block

    public static final byte b_EEFWDATA = (byte) 0x0A;  // IN/OUT Firmware Version & LUID (OUT is only LUID)

    public static final byte b_EEIDFRMTPARMS = (byte) 0x00;  // IN/OUT Card Format / Display parameters

    public static final byte b_EEXCOMBO = (byte) 0x04;  // IN/OUT Combo PcProx+ parameters

    public static final int MAXIMUM_FWFILENAME_LENGTH = 64; // The maximum string length of the Filename

    public static final int MAXIMUM_FWNAME_LENGTH = 64; //The maximum length of the buffer to get full firmware version

    public static final int TIMETOCOMMIT = 1200;

    public static final int TIMETOCOMMIT_FOR_LOON = 20;

    public static final byte b_SETFEATCMD_WRTEE = (byte) (0x80 | 0x10); // committ written data to storage

    public static final byte b_VRGETID32 = (byte) 0x0D; //InGetActiveID - 32 byte Big ID

    public static final byte b_VRGETIDDTL = (byte) 0x0E; //In GetLastIDDetail

}

package com.rfideas.pcproxapisdk;

import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags2;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags3;
import com.rfideas.pcproxapisdk.configuration.EEX1Config;
import com.rfideas.pcproxapisdk.configuration.IdBitCounts;
import com.rfideas.pcproxapisdk.configuration.IdDisplayParameters;
import com.rfideas.pcproxapisdk.configuration.IdDisplayParameters2;
import com.rfideas.pcproxapisdk.configuration.IdDisplayParameters3;
import com.rfideas.pcproxapisdk.configuration.LEDControl;
import com.rfideas.pcproxapisdk.configuration.TimeParameters;


/**
 * Base class for Configurations of a reader. Contains methods to alter
 * parameters in configuration of a reader.
 */
public class Configuration {

    private ConfigurationFlags configurationFlags;
    private ConfigurationFlags2 configurationFlags2;
    private ConfigurationFlags3 configurationFlags3;

    private IdDisplayParameters idDisplayParameters;
    private IdDisplayParameters2 idDisplayParameters2;
    private IdDisplayParameters3 idDisplayParameters3;

    private IdBitCounts idBitCounts;
    private TimeParameters timeParameters;
    private EEX1Config eex1Config;

    private int cardType;
    private int cardPriority;

    protected Configuration()
    {
        configurationFlags = new ConfigurationFlags();
        configurationFlags2 = new ConfigurationFlags2();
        configurationFlags3 = new ConfigurationFlags3();

        idDisplayParameters = new IdDisplayParameters();
        idDisplayParameters2 = new IdDisplayParameters2();
        idDisplayParameters3 = new IdDisplayParameters3();

        idBitCounts = new IdBitCounts();
        timeParameters = new TimeParameters();
        eex1Config = new EEX1Config();
    }

    protected int getCardPriority() {
        return cardPriority;
    }

    protected void setCardPriority(int cardPriority) { this.cardPriority = cardPriority; }

    protected int getCardType() {
        return cardType;
    }

    protected void setCardType(int cardType) {
        this.cardType = cardType;
    }

    protected ConfigurationFlags getConfigurationFlags() {
        return configurationFlags;
    }

    protected void setConfigurationFlags(ConfigurationFlags configurationFlags) {
        this.configurationFlags = configurationFlags;
    }

    protected ConfigurationFlags2 getConfigurationFlags2() {
        return configurationFlags2;
    }

    protected void setConfigurationFlags2(ConfigurationFlags2 configurationFlags2) {
        this.configurationFlags2 = configurationFlags2;
    }

    protected ConfigurationFlags3 getConfigurationFlags3() {
        return configurationFlags3;
    }

    protected void setConfigurationFlags3(ConfigurationFlags3 configurationFlags3) {
        this.configurationFlags3 = configurationFlags3;
    }

    protected IdDisplayParameters getIdDisplayParameters() {
        return idDisplayParameters;
    }

    protected void setIdDisplayParameters(IdDisplayParameters idDisplayParameters) {
        this.idDisplayParameters = idDisplayParameters;
    }

    protected IdDisplayParameters2 getIdDisplayParameters2() {
        return idDisplayParameters2;
    }

    protected void setIdDisplayParameters2(IdDisplayParameters2 idDisplayParameters2) {
        this.idDisplayParameters2 = idDisplayParameters2;
    }

    protected IdDisplayParameters3 getIdDisplayParameters3() {
        return idDisplayParameters3;
    }

    protected void setIdDisplayParameters3(IdDisplayParameters3 idDisplayParameters3) {
        this.idDisplayParameters3 = idDisplayParameters3;
    }

    protected IdBitCounts getIdBitCounts() {
        return idBitCounts;
    }

    protected void setIdBitCounts(IdBitCounts idBitCounts) {
        this.idBitCounts = idBitCounts;
    }

    protected TimeParameters getTimeParameters() {
        return timeParameters;
    }

    protected void setTimeParameters(TimeParameters timeParameters) {
        this.timeParameters = timeParameters;
    }

    protected EEX1Config getEex1Config() {
        return eex1Config;
    }

    protected void setEex1Config(EEX1Config eex1Config) {
        this.eex1Config = eex1Config;
    }
}

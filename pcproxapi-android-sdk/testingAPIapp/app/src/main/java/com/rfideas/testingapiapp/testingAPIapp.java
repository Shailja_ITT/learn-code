package com.rfideas.testingapiapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rfideas.pcproxapisdk.Enumerate;
import com.rfideas.pcproxapisdk.Reader;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags;
import com.rfideas.pcproxapisdk.configuration.ConfigurationFlags2;
import com.rfideas.pcproxapisdk.configuration.IdBitCounts;
import com.rfideas.pcproxapisdk.configuration.TimeParameters;


public class testingAPIapp extends AppCompatActivity {

    private Reader reader[];
    TextView apiOutput;
    Button checkApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing_apiapp);

        apiOutput = (TextView) findViewById(R.id.APIoutput);

        checkApi = (Button) findViewById(R.id.CheckApiButton);

        try {
            checkApi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TestAPI(v);
                }
            });
        }catch (Exception exception){
            Toast.makeText(this, exception.getMessage() , Toast.LENGTH_LONG).show();
        }
    }

    public void TestAPI(View v) {

        try {
            reader = Enumerate.USBConnect(this);
            if (Enumerate.GetDevCnt() == 0) {
                apiOutput.setText("No Reader Connected");
                return;
            }

            EditText readerNoText = (EditText) findViewById(R.id.reader);
            String readerNoString = readerNoText.getText().toString();
            int readerNo = Integer.parseInt(readerNoString);
            readerNo--;
            if (readerNo < 0) {
                readerNo = 0;
            }

            EditText APINameText = (EditText) findViewById(R.id.api);
            String APIName = APINameText.getText().toString();

            EditText args1Text = (EditText) findViewById(R.id.arg1);
            String args1 = args1Text.getText().toString();

            EditText args2Text = (EditText) findViewById(R.id.arg2);
            String args2 = args2Text.getText().toString();

            switch (APIName) {

                case "GetDevCnt":
                    int totalNoOfDevices = Enumerate.GetDevCnt();
                    Toast.makeText(this, "Total No of Devices: " + totalNoOfDevices, Toast.LENGTH_LONG).show();
                    break;

                case "GetMaxConfig":
                    int totalNoOfConfig = reader[readerNo].GetMaxConfig();
                    Toast.makeText(this, "Maximum Configuration: " + totalNoOfConfig, Toast.LENGTH_LONG).show();
                    break;

                case "BeepNow":
                    //TODO: Remove this try catch block
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                    byte count = Byte.parseByte(args1);
                    boolean longBeep = Boolean.parseBoolean(args2);

                    reader[readerNo].BeepNow(count, longBeep);

                    break;

                case "getPartNumberString":
                    String partNo = reader[readerNo].getPartNumberString();
                    Toast.makeText(this, "PartNumber: " + partNo, Toast.LENGTH_LONG).show();
                    break;

                case "ReadCfg":
                    int configIndex = Integer.parseInt(args1);
                    reader[readerNo].ReadCfg(configIndex);
                    break;

                case "GetCardType":
                    int configurationIndex = Integer.parseInt(args1);
                    int cardType = reader[readerNo].GetCardType(configurationIndex);
                    String cardTypeInHex = Integer.toHexString(cardType);
                    Toast.makeText(this, "CardType: " + cardTypeInHex.toUpperCase(), Toast.LENGTH_LONG).show();
                    break;

                case "GetCardPriority":
                    int configurationIndex1 = Integer.parseInt(args1);
                    int cardPriority = reader[readerNo].GetCardPriority(configurationIndex1);
                    Toast.makeText(this, "CardPriority: " + cardPriority, Toast.LENGTH_LONG).show();
                    break;

                case "GetActiveID":
                    try {
                        Thread.sleep(250);
                    } catch (Exception e) {
                    }
                    short size = Short.parseShort(args2);
                    int[] buf = new int[size];
                    short bits = reader[readerNo].GetActiveID(buf, size);
                    if (bits == 0) {
                        Toast.makeText(this, "No id found, Please put card on the reader", Toast.LENGTH_LONG).show();
                    } else {
                        String buffer = "";
                        String temp = "";
                        int bytes_to_read = (bits + 7) / 8;
                        if (bytes_to_read < 8) {
                            bytes_to_read = 8;
                        }
                        for (int i = 0; i < bytes_to_read; i++) {
                            temp = Integer.toHexString(buf[i]);
                            buffer = buffer.concat(temp);
                            buffer = buffer.concat(" ");
                        }

                        Toast.makeText(this, bits + " Bits: " + buffer.toUpperCase(), Toast.LENGTH_LONG).show();
                    }
                    break;

                case "GetBeeperVolume":
                    short volumeLevel = reader[readerNo].GetBeeperVolume();
                    Toast.makeText(this, "VolumeLevel: " + volumeLevel , Toast.LENGTH_LONG).show();
                    break;

                case "SetBeeperVolume" :
                    byte volumeLevelToSet = Byte.parseByte(args1);
                    if(reader[readerNo].SetBeeperVolume(volumeLevelToSet)) {
                        //TODO: Fix this why config 0
                        reader[readerNo].WriteCfg(0);
                        Toast.makeText(this, "Volume level " + volumeLevelToSet + " successfully saved", Toast.LENGTH_LONG).show();
                    }
                    else
                        Toast.makeText(this, "Unable to set volume level" , Toast.LENGTH_LONG).show();
                    break;

                case "GetIDBitCnts":
                    int configIndex1 = Integer.parseInt(args1);
                    reader[readerNo].ReadCfg(configIndex1);

                    IdBitCounts idBitCounts = reader[readerNo].GetIDBitCnts(configIndex1);
                    Toast.makeText(this, "For configuration " + configIndex1
                            + "\nValue of LP " + idBitCounts.getiLeadParityBitCnt()
                            + "\nValue of TP " + idBitCounts.getiTrailParityBitCnt()
                            + "\nValue of ID Bit Count " + idBitCounts.getiIDBitCnt()
                            + "\nValue of Total Bit Count " + idBitCounts.getiTotalBitCnt(), Toast.LENGTH_LONG).show();
                    break;

                case "GetFlags":
                    int configIndex_GetFlags = Integer.parseInt(args1);
                    reader[readerNo].ReadCfg(configIndex_GetFlags);

                    ConfigurationFlags configurationFlags = reader[readerNo].GetFlags(configIndex_GetFlags);
                    Toast.makeText(this, "For configuration " + configIndex_GetFlags
                            + "\nValue of bFixLenDsp " + configurationFlags.getbFixLenDsp()
                            + "\nValue of bFrcBitCntEx " + configurationFlags.getbFrcBitCntEx()
                            + "\nValue of bStripFac " + configurationFlags.getbStripFac()
                            + "\nValue of bSndFac " + configurationFlags.getbSndFac()
                            + "\nValue of bUseDelFac2Id " + configurationFlags.getbUseDelFac2Id()
                            + "\nValue of bNoUseELChar " + configurationFlags.getbNoUseELChar()
                            + "\nValue of bSndOnRx " + configurationFlags.getbSndOnRx()
                            + "\nValue of bHaltKBSnd " + configurationFlags.getbHaltKBSnd(), Toast.LENGTH_LONG).show();
                    break;

                case "GetFlags2":
                    int configIndex_GetFlags2 = Integer.parseInt(args1);
                    reader[readerNo].ReadCfg(configIndex_GetFlags2);

                    ConfigurationFlags2 configurationFlags2 = reader[readerNo].GetFlags2(configIndex_GetFlags2);
                    Toast.makeText(this, "For configuration " + configIndex_GetFlags2
                            + "\nValue of bUseLeadChrs " + configurationFlags2.getbUseLeadChrs()
                            + "\nValue of bDspHex " + configurationFlags2.getbDspHex()
                            + "\nValue of bWiegInvData " + configurationFlags2.getbWiegInvData()
                            + "\nValue of bUseInvDataF " + configurationFlags2.getbUseInvDataF()
                            + "\nValue of bRevWiegBits " + configurationFlags2.getbRevWiegBits()
                            + "\nValue of bBeepID " + configurationFlags2.getbBeepID()
                            + "\nValue of bRevBytes " + configurationFlags2.getbRevBytes(), Toast.LENGTH_LONG).show();
                    break;

                case "GetTimeParms":
                    int configIndex_GetTimeParms = Integer.parseInt(args1);
                    reader[readerNo].ReadCfg(configIndex_GetTimeParms);

                    TimeParameters timeParameters = reader[readerNo].GetTimeParms(configIndex_GetTimeParms);
                    Toast.makeText(this, "For configuration " + configIndex_GetTimeParms
                            + "\nValue of iBitStrmTO " + timeParameters.getiBitStrmTO()
                            + "\nValue of iIDHoldTO " + timeParameters.getiIDHoldTO()
                            + "\nValue of iIDLockOutTm " + timeParameters.getiIDLockOutTm()
                            + "\nValue of iUSBKeyPrsTm " + timeParameters.getiUSBKeyPrsTm()
                            + "\nValue of iUSBKeyRlsTm " + timeParameters.getiUSBKeyRlsTm()
                            + "\nValue of ExFeatures01 " + timeParameters.getExFeatures01()
                            + "\nValue of iTPCfgFlg3 " + timeParameters.getiTPCfgFlg3(), Toast.LENGTH_LONG).show();
                    break;

                case "GetFWFilename":
                    String FWFilename = reader[readerNo].GetFWFilename();
                    if(reader[readerNo].IsLoon())
                        Toast.makeText(this, " FWFileName: " + FWFilename, Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(this, " Not a Loon Reader", Toast.LENGTH_LONG).show();
                    break;

                case "GetFullFWVersion":
                    String fullFWVersion = reader[readerNo].GetFullFWVersion();
                    if(reader[readerNo].IsLoon())
                        Toast.makeText(this, " FullFWVersion: " + fullFWVersion, Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(this, " Not a loon reader", Toast.LENGTH_LONG).show();
                    break;

                case "GetDevName":
                    String devicePath = reader[readerNo].GetDevName();
                    Toast.makeText(this, "GetDevName: " + devicePath , Toast.LENGTH_LONG).show();
                    break;

                case "USBDisconnect":
                    Enumerate.USBDisconnect();
                    Toast.makeText(this, "All connected devices are disconnected", Toast.LENGTH_LONG).show();
                    break;

                case "SetFlags":
                    int configIndex_SetFlags = Integer.parseInt(args1);
                    boolean haltKBSnd = Boolean.parseBoolean(args2);
                    reader[readerNo].ReadCfg(configIndex_SetFlags);

                    ConfigurationFlags cfgFlags = reader[readerNo].GetFlags(configIndex_SetFlags);

                    cfgFlags.setbHaltKBSnd(haltKBSnd);

                    reader[readerNo].SetFlags(cfgFlags,configIndex_SetFlags);
                    reader[readerNo].WriteCfg(configIndex_SetFlags);

                    reader[readerNo].ReadCfg(configIndex_SetFlags);
                    cfgFlags = reader[readerNo].GetFlags(configIndex_SetFlags);
                    Toast.makeText(this, "Value of HaltKBSnd: " + cfgFlags.getbHaltKBSnd() + " is set in the reader", Toast.LENGTH_LONG).show();
                    break;

                default:
                    Toast.makeText(this, "Wrong API Passed", Toast.LENGTH_LONG).show();
            }
        }catch (Exception exception){
            Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
        }
        finally {
            Enumerate.USBDisconnect();
        }
    }
}

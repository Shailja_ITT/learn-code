
#include <iostream>
using namespace std;

struct node
{
	int popularity;
	struct node *next;
};
struct node* Head = NULL;

class Friend
{
	int testCases, numberOfFriends, numberOfFriendsToDelete;
public:
	int getNumberOfTestCase()
	{
		cin >> testCases;
			return testCases;
	}
	int getTotalFriend()
	{
		cin >> numberOfFriends;
		return numberOfFriends;
	}
	int getNumberOfFriendsToRemove()
	{
		cin >> numberOfFriendsToDelete;
		return numberOfFriendsToDelete;
	}
	
	void AddFriend(int numberOfFiends)
	{
		Head = NULL;
		node *currentFriendList, *newFriend;
		while (numberOfFiends--)
		{
			currentFriendList = Head;
			newFriend = new (node);
			cin >> newFriend->popularity;
			newFriend->next = NULL;
			
			if (Head == NULL)
			{
				Head = newFriend;
				Head->next = NULL;
			}
			else
			{
				while (currentFriendList->next != NULL)
				{
					currentFriendList = currentFriendList->next;
				}
				newFriend->next = NULL;
				currentFriendList->next = newFriend;
			}
		}

	}

	void RemoveFriend(int friendsToBeDeleted)
	{
		int flag = 0;
		node *currentFriend, *prevCurrentFriend;
		while (friendsToBeDeleted--)
		{
			prevCurrentFriend = Head;
			currentFriend = Head->next;
			if (Head->popularity < currentFriend->popularity)
			{
				Head = currentFriend;
				flag = 1;
			}
			else
			{
				while (currentFriend->next != NULL)
				{
					if (currentFriend->popularity < currentFriend->next->popularity)
					{
						prevCurrentFriend->next = currentFriend->next;
						flag = 1;
						break;
					}
					prevCurrentFriend = currentFriend;
					currentFriend = currentFriend->next;
				}
			}
			if (flag == 0)
			{
				prevCurrentFriend = Head;
				while (prevCurrentFriend->next->next != NULL)
					prevCurrentFriend = prevCurrentFriend->next;
					prevCurrentFriend->next = NULL;
			}
		}
	}

	void PrintRemainingFriend()
	{
		node *currentFriend;
		if (Head == NULL)
		{
			cout << "\nThe list is empty\n";
			return;
		}
		currentFriend = Head;
		while (currentFriend != NULL)
		{
			cout << currentFriend->popularity;
			cout << " ";
			currentFriend = currentFriend->next;
		}
		cout<<"\n";
	}

};

int main()
{
	Friend friendObject;
	int numberOfTestCases = friendObject.getNumberOfTestCase();
	while (numberOfTestCases--)
	{
		int totalFriends = friendObject.getTotalFriend();
		int friendsToBeRemoved = friendObject.getNumberOfFriendsToRemove();
		friendObject.AddFriend(totalFriends);
		friendObject.RemoveFriend(friendsToBeRemoved);
		friendObject.PrintRemainingFriend();
	}
	return 0;
}


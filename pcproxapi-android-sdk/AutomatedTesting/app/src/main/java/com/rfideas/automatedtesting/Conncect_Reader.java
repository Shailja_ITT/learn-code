package com.rfideas.automatedtesting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.rfideas.pcproxapisdk.Enumerate;
import com.rfideas.pcproxapisdk.SDKException;
import com.rfideas.pcproxapisdk.Reader;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class Conncect_Reader extends AppCompatActivity {
    Button connectButton;
    TextView partNumberText;
    private Reader[] readers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conncect__reader);
        connectButton = findViewById(R.id.Connectbutton);
        partNumberText = findViewById(R.id.PartNumberText);

            connectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View V) {
                    try {
                        TestAPI(V);
                    } catch (SDKException e) {
                        e.printStackTrace();
                    }
                }
            });


    }

    public void TestAPI(View V) throws SDKException {

            readers = Enumerate.USBConnect(this);
            partNumberText.setText(readers[0].getPartNumberString());
            Toast.makeText(this, readers[0].getPartNumberString(), Toast.LENGTH_LONG).show();

    }
}

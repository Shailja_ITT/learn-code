package com.rfideas.pcproxapisdk.configuration;

/**
 * Contains methods to alter variables defined under GetFlags() API
 */
public class EEX1Config {
    private short LeadParityX; // currently Expanded Leading Parity BitCount
    private short TrailParityX; // currently Expanded Trailing Parity BitCount
    private short iPad2;
    private short iPad3;
    private short iPad4;
    private short iPad5;
    private short iPad6;
    private short iPad7;

    public short getLeadParityX() {
        return LeadParityX;
    }

    public void setLeadParityX(short leadParityX) {
        LeadParityX = leadParityX;
    }

    public short getTrailParityX() {
        return TrailParityX;
    }

    public void setTrailParityX(short trailParityX) {
        TrailParityX = trailParityX;
    }

    public short getiPad2() {
        return iPad2;
    }

    public void setiPad2(short iPad2) {
        this.iPad2 = iPad2;
    }

    public short getiPad3() {
        return iPad3;
    }

    public void setiPad3(short iPad3) {
        this.iPad3 = iPad3;
    }

    public short getiPad4() {
        return iPad4;
    }

    public void setiPad4(short iPad4) {
        this.iPad4 = iPad4;
    }

    public short getiPad5() {
        return iPad5;
    }

    public void setiPad5(short iPad5) {
        this.iPad5 = iPad5;
    }

    public short getiPad6() {
        return iPad6;
    }

    public void setiPad6(short iPad6) {
        this.iPad6 = iPad6;
    }

    public short getiPad7() {
        return iPad7;
    }

    public void setiPad7(short iPad7) {
        this.iPad7 = iPad7;
    }
}

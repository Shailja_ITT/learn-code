package com.rfideas.pcproxapisdk;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class UsbCommunication {

    private static UsbManager _usbManager;
    private ArrayList<UsbDevice> usbDeviceList;
    private ArrayList<UsbDeviceConnection> usbDeviceConnectionList;
    private int totalNumberOfUsbDevices;

    private Context _context;

    UsbCommunication(Context context){
        _context = context;
        totalNumberOfUsbDevices = 0;
        usbDeviceList = new ArrayList<UsbDevice>() ;
        usbDeviceConnectionList = new ArrayList<UsbDeviceConnection>();

    }

    public ArrayList<UsbDeviceConnection> connectUsbDevices(){
        //Opening the device
        _usbManager = (UsbManager) _context.getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = _usbManager.getDeviceList();
        UsbInterface writeIntf;
        UsbDeviceConnection writeConnection;
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        int index = 0;
        while(deviceIterator.hasNext()){
            UsbDevice device = deviceIterator.next();
            if (device.getVendorId() == HexConstants.VENDOR_ID) {
                if (device.getProductId() == HexConstants.PLUS_PRODUCT_ID | device.getProductId() == HexConstants.BLE_PRODUCT_ID) {
                    writeConnection = _usbManager.openDevice(device);
                    writeIntf = device.getInterface(0);
                    writeConnection.claimInterface(writeIntf, true);
                    usbDeviceList.add(device);
                    usbDeviceConnectionList.add(writeConnection);
                    index++;
                }
            }
        }

        totalNumberOfUsbDevices = index;
        //writeConnection = _usbManager.openDevice(usbDeviceList.get(0));
        //this.lockUsbInterface(usbDeviceList.get(0));
        return usbDeviceConnectionList;
    }

    public ArrayList<UsbDevice> getUsbDeviceList() {
        return usbDeviceList;
    }
    /*public boolean lockUsbInterface(UsbDevice _usbDevice ){
        boolean rc = false;
        if (_usbDevice != null) {
            writeIntf = _usbDevice.getInterface(0);
            UsbEndpoint writeEp = writeIntf.getEndpoint(0);
            try {

                //System.out.println("rfideasError2" + writeConnection);
                writeConnection.claimInterface(writeIntf, true);
                System.out.println("rfideasError3" + writeConnection);
            }
            catch (NullPointerException npe) {
                writeConnection = _usbManager.openDevice(_usbDevice);
                writeConnection.claimInterface(writeIntf, true);
                System.out.println("rfideasError4" + writeConnection);
            }
                *//*writeConnection.claimInterface(writeIntf, true);*//*
            rc = true;
        }
        return rc;
    }*/

    /*public boolean disconnectUSBDevices(UsbDevice _usbDevice){
        boolean rc = false;
        if (_usbDevice != null) {
            writeConnection.releaseInterface(writeIntf);
            writeConnection.close();
            rc = true;
        }
        return rc;
    }*/

    //Returns length of data transferred (or zero) for success, or negative value for failure
    public int sendFR(byte[] buffer, UsbDeviceConnection usbDeviceConnection){
        int rc = 0;
        if (usbDeviceConnection != null) {
            rc = usbDeviceConnection.controlTransfer(UsbConstants.USB_DIR_OUT |
                            UsbConstants.USB_TYPE_CLASS |
                            UsbConstants.USB_INTERFACE_SUBCLASS_BOOT,
                    HexConstants.REQUEST_SET_REPORT, HexConstants.REPORT_TYPE_FEATURE, 0x0000,
                    buffer, buffer.length, 0);
        }
        return rc;
    }

    //Returns length of data transferred (or zero) for success, or negative value for failure
    public int getFR(byte[] buffer, UsbDeviceConnection usbDeviceConnection){
        int rc = 0;
        if (usbDeviceConnection != null) {
            rc = usbDeviceConnection.controlTransfer(UsbConstants.USB_DIR_IN |
                            UsbConstants.USB_TYPE_CLASS |
                            UsbConstants.USB_INTERFACE_SUBCLASS_BOOT,
                    HexConstants.REQUEST_GET_REPORT, HexConstants.REPORT_TYPE_FEATURE, 0x0000,
                    buffer, buffer.length, 0);
        }
        return rc;
    }

}

package com.rfideas.pcproxapisdk.configuration;

public class IdDisplayParameters2 {
    public short getiLeadChrCnt() {
        return iLeadChrCnt;
    }

    public void setiLeadChrCnt(short iLeadChrCnt) {
        this.iLeadChrCnt = iLeadChrCnt;
    }

    public short getiLeadChr0() {
        return iLeadChr0;
    }

    public void setiLeadChr0(short iLeadChr0) {
        this.iLeadChr0 = iLeadChr0;
    }

    public short getiLeadChr1() {
        return iLeadChr1;
    }

    public void setiLeadChr1(short iLeadChr1) {
        this.iLeadChr1 = iLeadChr1;
    }

    public short getiLeadChr2() {
        return iLeadChr2;
    }

    public void setiLeadChr2(short iLeadChr2) {
        this.iLeadChr2 = iLeadChr2;
    }

    public short getiCrdGnChr0() {
        return iCrdGnChr0;
    }

    public void setiCrdGnChr0(short iCrdGnChr0) {
        this.iCrdGnChr0 = iCrdGnChr0;
    }

    public short getiCrdGnChr1() {
        return iCrdGnChr1;
    }

    public void setiCrdGnChr1(short iCrdGnChr1) {
        this.iCrdGnChr1 = iCrdGnChr1;
    }

    private short iLeadChrCnt; // Tf bUseLeadChrs, contains the lead char count (<=3)
    private short iLeadChr0;   // These lead characters are filled in (up to 3)
    private short iLeadChr1;   // Leading character
    private short iLeadChr2;   // Leading character
    private short iCrdGnChr0;  // If non-zero, sent when ID goes Invalid
    private short iCrdGnChr1;

}
